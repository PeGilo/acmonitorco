﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo;

namespace ACMonitorCo.Test
{
    /// <summary>
    /// Summary description for MailingTest
    /// </summary>
    [TestClass]
    public class MailingTest
    {
        public MailingTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            MessageSettingsHandler messageSettigns = new MessageSettingsHandler();
            //messageSettigns.Server="mailtep.teploset.uprav.tgk13.ru";
            //messageSettigns.Port=25;
            //messageSettigns.User="teploset\aiistep";
            //messageSettigns.Password="0329";
            //messageSettigns.From="aiistep@teploset.tgk13.ru";
            //messageSettigns.Subject="АльфаЦентр - Ежедневный отчет";

            
            messageSettigns.Server = "t13-ehb102.corp.suek.ru";
            messageSettigns.Port = 25;
            messageSettigns.User = "suekcorp\\aiistep";
            messageSettigns.Password = "aI82eP00";
            messageSettigns.From = "aiistep@suek.ru";
            messageSettigns.Subject = "АльфаЦентр - Тест";
            

            RecipientCollection recipients = new RecipientCollection();
            //recipients.Add(new RecipientElement("GilayanPS@suek.ru", Level.Green));
            //recipients.Add(new RecipientElement("GilayanPS@suek.ru", Level.Yellow));
            recipients.Add(new RecipientElement("GilayanPS@suek.ru", Level.Red));

            //Program.MailReport("GREEN REPORT", Level.Green, messageSettigns, recipients);
            //Program.MailReport("YELLOW REPORT", Level.Yellow, messageSettigns, recipients);
            //Program.MailReport("RED REPORT", Level.Red, messageSettigns, recipients);
            MailAddress from = new MailAddress(messageSettigns.From);
            SmtpClient client = new SmtpClient(messageSettigns.Server, messageSettigns.Port);

            foreach (RecipientElement recip in recipients)
            {
                // Посылать отчет только тем у кого затребованный уровень 
                // включает в себя уровень текущего отчета
                //if (recip.Level <= reportLevel)
                //{
                    MailAddress to = new MailAddress(recip.Email);
                    MailMessage msg = new MailMessage(from, to);
                    msg.Subject = messageSettigns.Subject;
                    msg.IsBodyHtml = true;
                    msg.Body = "Test";

                    client.UseDefaultCredentials = false;
                    string[] credentials = messageSettigns.User.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                    client.Credentials = new NetworkCredential(credentials[1], messageSettigns.Password, credentials[0]);

                    client.Send(msg);
                //}
            }
        }
    }
}
