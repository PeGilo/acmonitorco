﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACMonitorCo.Test.Serialization
{
    /// <summary>
    /// Summary description for XmlEventSerializationTest
    /// </summary>
    [TestClass]
    public class XmlEventSerializationTest
    {
        public XmlEventSerializationTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            XmlEvent xe = new XmlEvent("date", "message", 1, Level.Green);

            Stream stream = new MemoryStream();
            
            XmlSerializer ser = new XmlSerializer(typeof(XmlEvent));
            ser.Serialize(stream, xe);

            byte[] buf = new byte[stream.Length];

            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(buf, 0, (int)stream.Length);

            Encoding.Unicode.GetString(buf);

            stream.Close();
        }

        [TestMethod]
        public void TestMethod2()
        {
            XmlDataEvent xde = new XmlDataEvent("date", "message", "period", 1, Level.Green);

            Stream stream = new MemoryStream();

            XmlSerializer ser = new XmlSerializer(typeof(XmlDataEvent));
            ser.Serialize(stream, xde);

            byte[] buf = new byte[stream.Length];

            stream.Seek(0, SeekOrigin.Begin);
            stream.Read(buf, 0, (int)stream.Length);

            Encoding.Unicode.GetString(buf);

            stream.Close();
        }
    }
}
