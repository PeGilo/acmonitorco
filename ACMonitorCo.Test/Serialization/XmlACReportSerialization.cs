﻿using System;
using System.Text;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACMonitorCo.Test.Serialization
{
    /// <summary>
    /// Summary description for XmlACReportSerialization
    /// </summary>
    [TestClass]
    public class XmlACReportSerialization
    {
        public XmlACReportSerialization()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            //List<XmlDataEvent> xde = new List<XmlDataEvent>{ 
            //    new XmlDataEvent("date1", "message1", "period1", 1),
            //    new XmlDataEvent("date1", "message1", "period1", 1),
            //    new XmlDataEvent("date1", "message1", "period1", 1),
            //    new XmlDataEvent("date1", "message1", "period1", 1)
            //};

            //List<XmlEvent> xe = new List<XmlEvent>{ 
            //    new XmlEvent("date1", "message1", 1),
            //    new XmlEvent("date1", "message1", 1),
            //    new XmlEvent("date1", "message1", 1),
            //    new XmlEvent("date1", "message1", 1)
            //};

            //XmlACReport report = new XmlACReport(xe.ToArray(), xde.ToArray());
            //report.Title = "Суточный отчет АСКУЭ \"АльфаЦЕНТР\" за";
            //report.Date = "2010-05-28";

            //report.Events.Title = "События счетчиков";
            //report.Events.EmptyMessage = "События о переключениях или пропадании фаз отсутствуют";

            //report.DataEvents.Title = "Данные счетчиков";
            //report.DataEvents.EmptyMessage = "Данные со счетчиков получены по всем интервала";

            //Stream stream = new MemoryStream();

            //XmlSerializer ser = new XmlSerializer(typeof(XmlACReport));
            //ser.Serialize(stream, report);

            //byte[] buf = new byte[stream.Length];

            //stream.Seek(0, SeekOrigin.Begin);
            //stream.Read(buf, 0, (int)stream.Length);

            //Encoding.UTF8.GetString(buf);

            //stream.Close();
        }

        [TestMethod]
        public void TestEmptySerialize()
        {
            //List<XmlDataEvent> xde = new List<XmlDataEvent>();

            //List<XmlEvent> xe = new List<XmlEvent>();

            //XmlACReport report = new XmlACReport(xe.ToArray(), xde.ToArray());
            //report.Title = "Суточный отчет АСКУЭ \"АльфаЦЕНТР\" за";
            //report.Date = "2010-05-28";

            //report.Events.Title = "События счетчиков";
            //report.Events.EmptyMessage = "События о переключениях или пропадании фаз отсутствуют";

            //report.DataEvents.Title = "Данные счетчиков";
            //report.DataEvents.EmptyMessage = "Данные со счетчиков получены по всем интервала";

            //Stream stream = new MemoryStream();

            //XmlSerializer ser = new XmlSerializer(typeof(XmlACReport));
            //ser.Serialize(stream, report);

            //byte[] buf = new byte[stream.Length];

            //stream.Seek(0, SeekOrigin.Begin);
            //stream.Read(buf, 0, (int)stream.Length);

            //Encoding.UTF8.GetString(buf);

            //stream.Close();
        }

    }
}
