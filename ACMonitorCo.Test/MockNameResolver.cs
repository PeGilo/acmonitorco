﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Test.Serialization
{
    class MockNameResolver : IShNameResolver
    {
        public MockNameResolver()
        {
        }

        public string GetName(Int32 nsh)
        {
            return "(тест) ЭК " + nsh.ToString();
        }
    }
}
