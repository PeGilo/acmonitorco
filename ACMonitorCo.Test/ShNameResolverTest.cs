﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Test
{
    /// <summary>
    /// Summary description for ShNameResolverTest
    /// </summary>
    [TestClass]
    public class ShNameResolverTest
    {
        public ShNameResolverTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestEmpty()
        {
            List<SHTXTItem> emptyList = new List<SHTXTItem>();

            ShNameResolver resolver = new ShNameResolver(emptyList);

            Assert.AreEqual(String.Empty, resolver.GetName(1));
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void TestNull()
        {
            ShNameResolver resolver = new ShNameResolver(null);
        }

        [TestMethod]
        public void TestMethod1()
        {
            List<SHTXTItem> shtxtItems = new List<SHTXTItem>() { 
                new SHTXTItem(1, "первый"),
                new SHTXTItem(2, "второй"), 
                new SHTXTItem(3, "третий"), 
                new SHTXTItem(4, "четвертый") 
            };

            ShNameResolver resolver = new ShNameResolver(shtxtItems);

            Assert.AreEqual("первый", resolver.GetName(1));
            Assert.AreEqual("второй", resolver.GetName(2));
            Assert.AreEqual("третий", resolver.GetName(3));
            Assert.AreEqual("четвертый", resolver.GetName(4));
            Assert.AreEqual(String.Empty, resolver.GetName(0));
        }
    }
}
