﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ACMonitorCo.Test
{
    /// <summary>
    /// Summary description for ParseReportDayTest
    /// </summary>
    [TestClass]
    public class ParseReportDayTest
    {
        public ParseReportDayTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestMethod1()
        {
            DateTime now = DateTime.Now;
            Assert.IsTrue(DatesAreEqual(now.AddDays(-1), Program.ParseReportDay("1")));
            Assert.IsTrue(DatesAreEqual(now.AddDays(-2), Program.ParseReportDay("-2")));
            Assert.IsTrue(DatesAreEqual(
                new DateTime(2010, 01, 31), 
                Program.ParseReportDay("31.01.2010")));
            Assert.IsTrue(DatesAreEqual(
                new DateTime(2010, 06, 28),
                Program.ParseReportDay("28/06/2010")));
        }

        public bool DatesAreEqual(DateTime a, DateTime b)
        {
            return (a.Year == b.Year
                && a.Month == b.Month
                && a.Day == b.Day);
        }
    }
}
