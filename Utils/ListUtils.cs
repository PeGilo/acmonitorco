﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utils
{
    public static class ListUtils
    {
        /// <summary>
        /// Removes items from list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="list">The list that items are removed from</param>
        /// <param name="toDelete">The items that are removed</param>
        //public static void RemoveItems<T>(LinkedList<T> list, IEnumerable<T> toDelete)
        //{
        //    foreach (var el in toDelete)
        //    {
        //        list.Remove(el);
        //    }
        //}

        public static void RemoveItems<T>(this LinkedList<T> list, IEnumerable<T> toDelete)
        {
            T[] array = toDelete.ToArray<T>();
            for(int i = 0; i < array.Length; i++)
            {
                list.Remove(array[i]);
            }
        }
    }
}
