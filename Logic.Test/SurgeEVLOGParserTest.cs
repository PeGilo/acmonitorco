﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    ///This is a test class for SurgeEVLOGParserTest and is intended
    ///to contain all SurgeEVLOGParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class SurgeEVLOGParserTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Parse
        ///</summary>
        [TestMethod()]
        public void TestParseCorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            // Correct event
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 12), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 16), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 15), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 14), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 13), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 12), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 11), 211));

            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.AreEqual(1155843, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 04, 13, 13, 51, 21, 11), ev.Time);
        }

        [TestMethod()]
        public void TestParseCorrect2()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            // Correct event

            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 15, 11), 208));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 15, 12), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 15, 13), 212));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 14), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 15), 211));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 16), 209));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 17), 213));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 18), 212));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 13, 13, 51, 12, 19), 208));

            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.AreEqual(1155842, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 04, 13, 13, 51, 12, 19), ev.Time);
        }

        [TestMethod()]
        public void TestParseCorrect3()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            // Correct event

            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 211));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 212));

            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.AreEqual(1155843, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 02, 19, 16, 47, 00, 11), ev.Time);
        }

        [TestMethod()]
        public void TestParseIncorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            
            // Incorrect event (включение)
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 209));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 209));

            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            // parser must delete first item that it can't parse
            Assert.AreEqual(9, list.Count);
        }

        [TestMethod()]
        public void TestParseNullOrEmpty()
        {
            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            // Check reaction on null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check less then 9 items
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 16), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 15), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 14), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 13), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 12), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 11), 211));

            ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseIncorrectEventCode()
        {
            SurgeEVLOGParser parser = new SurgeEVLOGParser(null);

            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 12), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 16), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 15), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 14), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 13), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 12), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 11), 211));
            // This first item must make error
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 206));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

    }
}
