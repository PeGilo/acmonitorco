﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for SeriesTest
    /// </summary>
    [TestClass]
    public class SeriesTest
    {
        public SeriesTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void Test1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Несколько включений / отключений
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 33, 11), 1));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 12, 11), 1));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 12, 11), 0));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 12, 57, 57, 11), 0));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 13, 07, 34, 11), 1));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 13, 07, 13, 11), 1));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 13, 07, 13, 11), 0));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 12, 57, 58, 11), 0));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 05, 28, 13, 07, 35, 11), 1));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 05, 28, 13, 07, 14, 11), 1));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 05, 28, 13, 07, 14, 11), 0));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 05, 28, 12, 58, 00, 11), 0));

            EVLOGParser itemParser = new PowerOffEVLOGParser(
                                             new PowerOnEVLOGParser(null));
            List<Event> resultList = new List<Event>();

            Event ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    resultList.Add(ev);
            }

            Assert.AreEqual(6, resultList.Count);
            foreach (Event e in resultList)
            {
                Assert.IsInstanceOfType(e, typeof(PowerFailureEvent));
            }
        }

        [TestMethod]
        public void Test2()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Отключение
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 30, 49, 11), 206));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 30, 49, 11), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 30, 49, 11), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 30, 49, 11), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 28, 27, 11), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 28, 27, 11), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 11, 28, 27, 11), 212));
            // Включение
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 40, 31, 11), 207));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 40, 31, 11), 209));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 40, 31, 11), 211));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 40, 31, 11), 213));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 208));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 209));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 211));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 210));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 212));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 07, 10, 33, 56, 11), 213));

            EVLOGParser itemParser = new SwitchOnEVLOGParser(
                                             new SwitchOffEVLOGParser(null));
            List<Event> resultList = new List<Event>();

            Event ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    resultList.Add(ev);
            }

            Assert.AreEqual(2, resultList.Count);
            foreach (Event e in resultList)
            {
                Assert.IsInstanceOfType(e, typeof(SwitchingEvent));
            }
            Assert.AreEqual(Switching.On, ((SwitchingEvent)resultList[0]).Operation);
            Assert.AreEqual(Switching.Off, ((SwitchingEvent)resultList[1]).Operation);
        }

        [TestMethod]
        public void Test3()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Кратковременное включение
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 211));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 212));
            // Кратковременное включение
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 211));
            // Отключение
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 206));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 208));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 210));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 212));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 208));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 210));
            list.AddFirst(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 212));

            EVLOGParser itemParser = new SurgeEVLOGParser(
                                        new SwitchOnEVLOGParser(
                                             new SwitchOffEVLOGParser(null)));
            List<Event> resultList = new List<Event>();

            Event ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    resultList.Add(ev);
            }

            Assert.AreEqual(3, resultList.Count);

            Assert.IsInstanceOfType(resultList[0], typeof(SwitchingEvent));
            Assert.IsInstanceOfType(resultList[1], typeof(SurgeEvent));
            Assert.IsInstanceOfType(resultList[2], typeof(SurgeEvent));

            Assert.AreEqual(Switching.Off, ((SwitchingEvent)resultList[0]).Operation);
        }

        [TestMethod]
        public void Test4()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Отключение, которое ошибочно принимается за асинхронное, потому что в разных секундах события
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 14), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 13), 206));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 12), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 30, 00), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 30, 00), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 29, 00), 208));


            EVLOGParser itemParser = new SurgeEVLOGParser(
                                         new SwitchOnEVLOGParser(
                                             new SwitchOffEVLOGParser(
                                                 new PhaseFailureEVLOGParser(
                                                     new PowerOffEVLOGParser(
                                                         new PowerOnEVLOGParser(null))))));
            List<Event> resultList = new List<Event>();

            Event ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    resultList.Add(ev);
            }

            Assert.AreEqual(1, resultList.Count);
            foreach (Event e in resultList)
            {
                Assert.IsInstanceOfType(e, typeof(SwitchingEvent));
            }
        }

        [TestMethod]
        public void Test5()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 13), 3));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 12), 2));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 10, 05, 12, 00, 49, 11), 3));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 10, 05, 11, 57, 30, 00), 2));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 10, 05, 11, 57, 30, 00), 3));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 10, 05, 11, 57, 29, 00), 2));

            EVLOGParser itemParser = new SurgeEVLOGParser(
                                         new SwitchOnEVLOGParser(
                                             new SwitchOffEVLOGParser(
                                                 new PhaseFailureEVLOGParser(
                                                     new PowerOffEVLOGParser(
                                                         new PowerOnEVLOGParser(
                                                             new CounterTimeCorrectionEVLOGParser(null)))))));
            List<Event> resultList = new List<Event>();

            Event ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    resultList.Add(ev);
            }

            Assert.AreEqual(3, resultList.Count);
            foreach (Event e in resultList)
            {
                Assert.IsInstanceOfType(e, typeof(CounterTimeCorrectionEvent));
            }
        }

    }
}
