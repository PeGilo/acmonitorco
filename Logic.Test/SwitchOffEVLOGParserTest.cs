﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for SwitchOffEVLOGParserTest
    /// </summary>
    [TestClass]
    public class SwitchOffEVLOGParserTest
    {
        public SwitchOffEVLOGParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod()]
        public void TestParseIncorrectEventCode()
        {
            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 209));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 209));
            // This first item must make error
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 206));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseNullOrEmpty()
        {
            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            // Check reaction on null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check less then 3 items
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 208));

            ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseIncorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Incorrect event (включение)
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 15), 209));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 13), 211));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 11), 213));

            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            // parser must delete first item that it can't parse
            Assert.AreEqual(2, list.Count);
        }

        [TestMethod()]
        public void TestParseIncorrect2()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Incorrect event (отключение фаз)
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 15), 211));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 13), 209));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 11), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 15), 208));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 13), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 11), 208));

            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            // parser must delete first item that it can't parse
            Assert.AreEqual(5, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (отключение без подтверждения)
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 54, 13), 208));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 54, 12), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 54, 11), 212));

            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155841, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 06, 13, 02, 54, 11), ev.Time);
            Assert.AreEqual(Switching.Off, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect2()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (отключение с подтверждением)
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 11, 02, 45, 17), 206));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 11, 02, 45, 16), 208));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 11, 02, 45, 15), 210));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 11, 02, 45, 14), 212));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 10, 50, 50, 13), 208));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 10, 50, 50, 12), 210));
            list.AddFirst(new EVLOGItem(1155853, new DateTime(2010, 05, 08, 10, 50, 50, 11), 212));

            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155853, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 08, 10, 50, 50, 11), ev.Time);
            Assert.AreEqual(Switching.Off, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(1, list.Count);
        }

//1155843                05-окт-2010 11:57:29                    208                    
//1155843                05-окт-2010 11:57:30                    210                    
//1155843                05-окт-2010 11:57:30                    212                    
//1155843                05-окт-2010 12:00:49                    206                    
//1155843                05-окт-2010 12:00:49                    208                    
//1155843                05-окт-2010 12:00:49                    210                    
//1155843                05-окт-2010 12:00:49                    212 

//1155843, 05.10.2010 11:57:29, 208
//1155843, 05.10.2010 11:57:30, 212
//1155843, 05.10.2010 11:57:30, 210
//1155843, 05.10.2010 12:00:49, 210
//1155843, 05.10.2010 12:00:49, 212
//1155843, 05.10.2010 12:00:49, 206
//1155843, 05.10.2010 12:00:49, 208

        [TestMethod()]
        public void TestParseCorrect3_DiffSeconds()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (отключение с подтверждением + секунды разные)
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 14), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 13), 206));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 12), 212)); 
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 12, 00, 49, 11), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 30, 00), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 30, 00), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 10, 05, 11, 57, 29, 00), 208));


            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155843, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 10, 05, 11, 57, 29, 00), ev.Time);
            Assert.AreEqual(Switching.Off, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(1, list.Count);
        }

//1155859                28-сен-2010 19:50:20                    206                    
//1155859                28-сен-2010 19:50:20                    208                    
//1155859                28-сен-2010 19:50:20                    210                    
//1155859                28-сен-2010 19:50:20                    212                    
//1155859                28-сен-2010 19:47:02                    212                    
//1155859                28-сен-2010 19:47:03                    208                    
//1155859                28-сен-2010 19:47:03                    210                    

        [TestMethod()]
        public void TestParseCorrect4_DiffSeconds()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (отключение с подтверждением + секунды разные)
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 50, 20, 17), 206));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 50, 20, 16), 208));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 50, 20, 15), 210));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 50, 20, 14), 212));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 47, 03, 00), 208));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 47, 03, 00), 210));
            list.AddFirst(new EVLOGItem(1155859, new DateTime(2010, 09, 28, 19, 47, 02, 00), 212));


            SwitchOffEVLOGParser parser = new SwitchOffEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155859, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 09, 28, 19, 47, 02, 00), ev.Time);
            Assert.AreEqual(Switching.Off, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(1, list.Count);
        }
    }
}
