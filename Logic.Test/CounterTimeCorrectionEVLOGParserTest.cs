﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for CounterTimeCorrectionEVLOGParserTest
    /// </summary>
    [TestClass]
    public class CounterTimeCorrectionEVLOGParserTest
    {
        LinkedList<EVLOGItem> _list = null;
        CounterTimeCorrectionEVLOGParser _parser = null;

        public CounterTimeCorrectionEVLOGParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize() {
            _list = new LinkedList<EVLOGItem>();
            _parser = new CounterTimeCorrectionEVLOGParser(null);
        }

        //
        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup() { }
        
        #endregion

        [TestMethod]
        public void SimpleTest1()
        {
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 3));
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 2));

            CounterEvent ev = _parser.Parse(_list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(CounterTimeCorrectionEvent));
            Assert.AreEqual(1155864, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 07, 18, 20, 42, 19), ev.Time);

            // check list after parsing
            Assert.AreEqual(0, _list.Count);
        }

        [TestMethod]
        public void SimpleTest2()
        {
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 2));
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 3));

            CounterEvent ev = _parser.Parse(_list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(CounterTimeCorrectionEvent));
            Assert.AreEqual(1155864, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 07, 18, 20, 42, 19), ev.Time);

            // check list after parsing
            Assert.AreEqual(0, _list.Count);
        }

        /// <summary>
        /// Список null
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            _list = null;

            Event ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Кол-во элементов списка меньше 1
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            _list.Clear();
            Event ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Первый элемент списка Null
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            _list.AddFirst((EVLOGItem)null);

            Event ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Код события первого элемента неправильный
        /// </summary>
        [TestMethod]
        public void Test4()
        {
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 1111));

            Event ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Завершающее событие неподходящее
        /// </summary>
        [TestMethod]
        public void Test5()
        {
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 1111));
            _list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 2));

            CounterEvent ev = _parser.Parse(_list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(CounterTimeCorrectionEvent));
            Assert.AreEqual(1155864, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 07, 18, 20, 42, 19), ev.Time);

            // check list after parsing
            Assert.AreEqual(1, _list.Count);
        }

    }
}
