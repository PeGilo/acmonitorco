﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for SwitchOnEVLOGParserTest
    /// </summary>
    [TestClass]
    public class SwitchOnEVLOGParserTest
    {
        public SwitchOnEVLOGParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod()]
        public void TestParseIncorrectEventCode()
        {
            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 209));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 209));
            // This first item must make error
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 206));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
        }


        [TestMethod()]
        public void TestParseNullOrEmpty()
        {
            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            // Check reaction on null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check less then 3 items
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 213));

            ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseIncorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Incorrect event (отключение)
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 208));

            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            // parser must delete first item that it can't parse
            Assert.AreEqual(9, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (включение без подтверждения)
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 16), 208));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 15), 209));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 14), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 13), 211));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 12), 212));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 02, 18, 11), 213));

            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155841, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 06, 13, 02, 18, 11), ev.Time);
            Assert.AreEqual(Switching.On, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect2()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (включение с подтверждением)
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 08, 00, 02, 04, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 08, 00, 02, 04, 19), 209));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 08, 00, 02, 04, 18), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 08, 00, 02, 04, 17), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 16), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 15), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 14), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 13), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 12), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 23, 56, 26, 11), 209));

            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155864, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 05, 07, 23, 56, 26, 11), ev.Time);
            Assert.AreEqual(Switching.On, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(1, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect3()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (кратковременное включение)
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 12), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 11), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 16), 208));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 15), 209));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 14), 210));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 13), 212));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 12), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 11), 211));

            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155843, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 04, 13, 13, 51, 21, 11), ev.Time);
            Assert.AreEqual(Switching.On, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(3, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect4()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Correct event (событие только из трех сообщений)
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 14), 207));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 13), 213));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 12), 211));
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 21, 11), 209));

            SwitchOnEVLOGParser parser = new SwitchOnEVLOGParser(null);

            CounterEvent ev = parser.Parse(list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(SwitchingEvent));
            Assert.AreEqual(1155843, ev.N_SH);
            Assert.AreEqual(new DateTime(2010, 04, 13, 13, 51, 21, 11), ev.Time);
            Assert.AreEqual(Switching.On, ((SwitchingEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(1, list.Count);
        }
    }
}
