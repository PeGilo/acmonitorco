﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for PowerFailureOnTest
    /// </summary>
    [TestClass]
    public class PowerFailureOnTest
    {
        public PowerFailureOnTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod()]
        public void TestParseIncorrectEventCode()
        {
            PowerOnEVLOGParser parser = new PowerOnEVLOGParser(null);

            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 33, 11), 1));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 12, 11), 1));
            // This item must make error
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseNullOrEmpty()
        {
            PowerOnEVLOGParser parser = new PowerOnEVLOGParser(null);

            // Check reaction on null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check less then 1 items
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseIncorrect1()
        {
            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Incorrect event (отключение)
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 208));

            PowerOnEVLOGParser parser = new PowerOnEVLOGParser(null);

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            // parser must delete first item that it can't parse
            Assert.AreEqual(9, list.Count);
        }

        [TestMethod()]
        public void TestParseCorrect1()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 33, 12), 1));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 28, 13, 07, 12, 11), 1));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect2()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 13, 10, 34, 12), 1));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 05, 28, 13, 07, 13, 11), 1));

            CheckCorrectnessWithZeroRemainder(list);
        }

        private void CheckCorrectnessWithZeroRemainder(LinkedList<EVLOGItem> list)
        {
            PowerOnEVLOGParser parser = new PowerOnEVLOGParser(null);

            // The first item must be start of event
            DateTime checkTime = list.First.Value.EVTIME;
            Int32 checkNSH = list.First.Value.N_SH;

            // parsing
            CounterEvent ev = parser.Parse(list);

            // check results
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(PowerFailureEvent));
            Assert.AreEqual(checkNSH, ev.N_SH);
            Assert.AreEqual(checkTime, ev.Time);
            Assert.AreEqual(Switching.On, ((PowerFailureEvent)ev).Operation);

            // check list after parsing
            Assert.AreEqual(0, list.Count, "Must remain zero items");
        }
    }
}
