﻿using ACMonitorCo.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ACMonitorCo.DataAccess;
using System.Collections.Generic;

namespace ACMonitorCo.Logic
{
    
    
    /// <summary>
    ///This is a test class for DataParserTest and is intended
    ///to contain all DataParserTest Unit Tests
    ///</summary>
    [TestClass()]
    public class DataParserTest
    {


        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        // 
        //You can use the following additional attributes as you write your tests:
        //
        //Use ClassInitialize to run code before running the first test in the class
        //[ClassInitialize()]
        //public static void MyClassInitialize(TestContext testContext)
        //{
        //}
        //
        //Use ClassCleanup to run code after all tests in a class have run
        //[ClassCleanup()]
        //public static void MyClassCleanup()
        //{
        //}
        //
        //Use TestInitialize to run code before running each test
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{
        //}
        //
        //Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{
        //}
        //
        #endregion


        /// <summary>
        ///A test for Parse
        ///</summary>
        [TestMethod()]
        public void ParseEmptyOrNull()
        {
            IEnumerable<Event> result = DataParser.Parse(new List<EVLOGItem>());
            Assert.AreNotEqual(null, result);
            foreach (Event e in result)
            {
                Assert.Fail("should not be any elements");
            }
        }
    }
}
