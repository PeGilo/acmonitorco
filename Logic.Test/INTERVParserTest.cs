﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.DataAccess;
using ACMonitorCo.Logic.Parsers;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for INTERVParserTest
    /// </summary>
    [TestClass]
    public class INTERVParserTest
    {
        public INTERVParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestNullOrEmpty()
        {
            INTERVParser parser = new INTERVParser();
            LinkedList<INTERVItem> events = new LinkedList<INTERVItem>();

            // Check for null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check for empty
            ev = parser.Parse(events);
            Assert.IsNull(ev);
        }

        [TestMethod]
        public void Test1()
        {
            INTERVParser parser = new INTERVParser();
            LinkedList<INTERVItem> events = new LinkedList<INTERVItem>();

            events.AddFirst(new INTERVItem(new DateTime(2010, 05, 28, 13, 45, 00), 1155865, 26, 750, 780));
            //events.AddFirst(new INTERVItem(new DateTime(2010, 05, 28, 13, 45, 00), 1155865, 27, 780, 810));

            Event ev = parser.Parse(events);
            Assert.IsNotNull(ev);
        }
    }
}
