﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for CounterTimeCorrectionEVLOGParserTest
    /// </summary>
    [TestClass]
    public class RtuTimeCorrectionEVLOGRTUParserTest
    {
        LinkedList<EVLOGRTUItem> _list = null;
        RtuTimeCorrectionEVLOGRTUParser _parser = null;

        public RtuTimeCorrectionEVLOGRTUParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }

        //
        // Use TestInitialize to run code before running each test 
        [TestInitialize()]
        public void MyTestInitialize() {
            _list = new LinkedList<EVLOGRTUItem>();
            _parser = new RtuTimeCorrectionEVLOGRTUParser(null);
        }

        //
        // Use TestCleanup to run code after each test has run
        [TestCleanup()]
        public void MyTestCleanup() { }
        
        #endregion

        [TestMethod]
        public void SimpleTest1()
        {
            _list.AddFirst(new EVLOGRTUItem(new DateTime(2010, 05, 07, 18, 20, 41, 20), 0, 104, "Завершение команды коррекции времени,УСПД:2359"));
            _list.AddFirst(new EVLOGRTUItem(new DateTime(2010, 05, 07, 18, 20, 42, 19), 0, 103, "Команда на коррекцию времени УСПД:2359(2 сек)"));

            RtuEvent ev = _parser.Parse(_list);
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(RtuTimeCorrectionEvent));
            Assert.AreEqual(2359, ev.RtuNumber);
            Assert.AreEqual(new DateTime(2010, 05, 07, 18, 20, 42, 19), ev.Time);

            // check list after parsing
            Assert.AreEqual(1, _list.Count);
        }

        /// <summary>
        /// Список null
        /// </summary>
        [TestMethod]
        public void Test1()
        {
            _list = null;

            RtuEvent ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Кол-во элементов списка меньше 1
        /// </summary>
        [TestMethod]
        public void Test2()
        {
            _list.Clear();
            RtuEvent ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Первый элемент списка Null
        /// </summary>
        [TestMethod]
        public void Test3()
        {
            _list.AddFirst((EVLOGRTUItem)null);

            RtuEvent ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

        /// <summary>
        /// Код события первого элемента неправильный
        /// </summary>
        [TestMethod]
        public void Test4()
        {
            _list.AddFirst(new EVLOGRTUItem(new DateTime(2010, 05, 07, 18, 20, 41, 20), 0, 104, "Завершение команды коррекции времени,УСПД:2359"));

            RtuEvent ev = _parser.Parse(_list);
            Assert.IsNull(ev);
        }

    }
}
