﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using ACMonitorCo.Logic.Parsers;
using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Summary description for PhaseFailureEVLOGParserTest
    /// </summary>
    [TestClass]
    public class PhaseFailureEVLOGParserTest
    {
        public PhaseFailureEVLOGParserTest()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod()]
        public void TestParseIncorrectEventCode()
        {
            PhaseFailureEVLOGParser parser = new PhaseFailureEVLOGParser(null);

            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 20), 207));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 19), 209));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 18), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 20, 42, 17), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 16), 208));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 15), 211));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 14), 213));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 212));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 210));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 209));
            // This first item must make error
            list.AddFirst(new EVLOGItem(1155843, new DateTime(2010, 04, 13, 13, 51, 24, 13), 206));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            Assert.AreEqual(10, list.Count, "Parser must remove unparsed item");
        }


        [TestMethod()]
        public void TestParseNullOrEmpty()
        {
            PhaseFailureEVLOGParser parser = new PhaseFailureEVLOGParser(null);

            // Check reaction on null
            Event ev = parser.Parse(null);
            Assert.IsNull(ev);

            // Check less then 1 items
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            ev = parser.Parse(list);
            Assert.IsNull(ev);
        }

        [TestMethod()]
        public void TestParseIncorrect1()
        {
            PhaseFailureEVLOGParser parser = new PhaseFailureEVLOGParser(null);

            // Events
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();

            // Incorrect events
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 13), 203));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 12), 202));
            list.AddFirst(new EVLOGItem(1155864, new DateTime(2010, 05, 07, 18, 16, 02, 11), 201));

            Event ev = parser.Parse(list);
            Assert.IsNull(ev);
            Assert.AreEqual(2, list.Count, "Parser must remove unparsed item");
        }

        [TestMethod()]
        public void TestParseCorrect1()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 20, 10, 12), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 20, 10, 11), 208));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect2()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 19, 19, 11), 208));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 19, 19, 11), 209));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 19, 19, 11), 211));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 14, 19, 19, 11), 210));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect3()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 55, 23, 11), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 55, 23, 11), 208));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect4()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 54, 26, 11), 208));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 54, 26, 11), 209));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 54, 26, 11), 210));
            list.AddFirst(new EVLOGItem(1155841, new DateTime(2010, 05, 06, 13, 54, 26, 11), 211));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect5()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 13, 40, 35, 11), 212));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 13, 31, 12, 11), 212));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect6()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 50, 33, 13), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 44, 14, 12), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 44, 14, 11), 211));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect7()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 37, 57, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 37, 55, 11), 210));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect8()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 36, 49, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 36, 48, 11), 210));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect9()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 34, 33, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 34, 27, 11), 210));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect10()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 32, 39, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 30, 04, 32, 37, 11), 210));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect11()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 29, 12, 57, 58, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 29, 12, 57, 58, 11), 211));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect12()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 29, 12, 57, 15, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 29, 12, 57, 08, 11), 210));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 04, 29, 12, 57, 08, 11), 211));

            CheckCorrectnessWithZeroRemainder(list);
        }

        [TestMethod()]
        public void TestParseCorrect13()
        {
            // Correct event (abnormal phase)
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>();
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 02, 23, 20, 30, 39, 11), 212));
            list.AddFirst(new EVLOGItem(1155842, new DateTime(2010, 02, 23, 20, 25, 25, 11), 212));

            CheckCorrectnessWithZeroRemainder(list);
        }

        private void CheckCorrectnessWithZeroRemainder(LinkedList<EVLOGItem> list)
        {
            PhaseFailureEVLOGParser parser = new PhaseFailureEVLOGParser(null);

            // The first item must be start of event
            DateTime checkTime = list.First.Value.EVTIME;
            Int32 checkNSH = list.First.Value.N_SH;

            // parsing
            CounterEvent ev = parser.Parse(list);

            // check results
            Assert.IsNotNull(ev);
            Assert.IsInstanceOfType(ev, typeof(PhaseFailureEvent));
            Assert.AreEqual(checkNSH, ev.N_SH);
            Assert.AreEqual(checkTime, ev.Time);

            // check list after parsing
            Assert.AreEqual(0, list.Count, "Must remain zero items");
        }


    }
}
