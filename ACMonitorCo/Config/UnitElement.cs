﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class Unit : ConfigurationElement
    {
        public Unit()
        {
        }

        public Unit(Int32 number)
        {
            Number = number;
        }

        [ConfigurationProperty("number", IsKey=true, IsRequired = true)]
        public Int32 Number
        {
            get
            { return (Int32)this["number"]; }
            set
            { this["number"] = value; }
        }
    }
}
