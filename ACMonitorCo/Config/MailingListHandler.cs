﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class MailingListHandler : ConfigurationSection
    {
        public MailingListHandler()
        {
        }

        //[ConfigurationProperty("recipient")]
        //public RecipientElement[] Recipient
        //{
        //    get
        //    { return (RecipientElement[])this["recipient"]; }
        //    set
        //    { this["recipient"] = value; }
        //}

        [ConfigurationProperty("recipients", IsDefaultCollection = false)]
        public RecipientCollection Recipients
        {
            get { return (RecipientCollection)base["recipients"]; }
        }
    }
}
