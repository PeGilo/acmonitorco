﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class UnitCollection : ConfigurationElementCollection
    {
        public UnitCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public Unit this[int index]
        {
            get { return (Unit)base.BaseGet(index); }
        }

        //public new Counter this[Int32 number]
        //{
        //    get { return (Counter)base.BaseGet(number); }
        //}

        protected override ConfigurationElement CreateNewElement()
        {
            return new Unit();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((Unit)element).Number;
        }

        public int IndexOf(Unit counter)
        {
            return BaseIndexOf(counter);
        }

        public bool ContainsCounterNumber(Int32 counterNumber)
        {
            for(int i = 0; i < Count; i++)
            {
                if (this[i].Number == counterNumber)
                    return true;
            }
            return false;
        }

        public void Add(Unit counter)
        {
            BaseAdd(counter);
        }

        public void Remove(Unit counter)
        {
            if (BaseIndexOf(counter) > 0)
            {
                BaseRemove(counter.Number);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(Int32 number)
        {
            BaseRemove(number);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
