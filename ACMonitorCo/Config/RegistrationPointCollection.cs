﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class RegistrationPointCollection : ConfigurationElementCollection
    {
        public RegistrationPointCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public RegistrationPointElement this[int index]
        {
            get { return (RegistrationPointElement)base.BaseGet(index); }
        }

        public new RegistrationPointElement this[String name]
        {
            get { return (RegistrationPointElement)base.BaseGet(name); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RegistrationPointElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RegistrationPointElement)element).Name;
        }

        public int IndexOf(RegistrationPointElement regPoint)
        {
            return BaseIndexOf(regPoint);
        }

        public void Add(RegistrationPointElement regPoint)
        {
            BaseAdd(regPoint);
        }

        public void Remove(RegistrationPointElement regPoint)
        {
            if (BaseIndexOf(regPoint) > 0)
            {
                BaseRemove(regPoint.Name);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(Int32 number)
        {
            BaseRemove(number);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
