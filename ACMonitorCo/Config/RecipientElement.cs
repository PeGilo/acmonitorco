﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class RecipientElement : ConfigurationElement
    {
        public RecipientElement()
        {
        }

        public RecipientElement(String email, Level level)
        {
            Email = email;
            Level = level;
        }

        [ConfigurationProperty("email", IsKey=true, IsRequired = true)]
        public String Email
        {
            get
            { return (String)this["email"]; }
            set
            { this["email"] = value; }
        }

        [ConfigurationProperty("level", IsRequired = true)]
        //[RegexStringValidator(@"red|green|yellow|Red|Green|Yellow")]
        public Level Level
        {
            get
            { return (Level)this["level"]; }
            set
            { this["level"] = value; }
        }
    }
}
