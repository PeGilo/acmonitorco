﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class RecipientCollection : ConfigurationElementCollection
    {
        public RecipientCollection()
        {
        }

        public override ConfigurationElementCollectionType CollectionType
        {
            get
            {
                return ConfigurationElementCollectionType.AddRemoveClearMap;
            }
        }

        public RecipientElement this[int index]
        {
            get { return (RecipientElement)base.BaseGet(index); }
        }

        public new RecipientElement this[string email]
        {
            get { return (RecipientElement)base.BaseGet(email); }
        }

        protected override ConfigurationElement CreateNewElement()
        {
            return new RecipientElement();
        }

        protected override object GetElementKey(ConfigurationElement element)
        {
            return ((RecipientElement)element).Email;
        }

        public int IndexOf(RecipientElement recipient)
        {
            return BaseIndexOf(recipient);
        }

        public void Add(RecipientElement recipient)
        {
            BaseAdd(recipient);
        }

        public void Remove(RecipientElement recipient)
        {
            if (BaseIndexOf(recipient) > 0)
            {
                BaseRemove(recipient.Email);
            }
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }
}
