﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class RegistrationPointElement : ConfigurationElement
    {
        public RegistrationPointElement()
        {
        }

        public RegistrationPointElement(String name)
        {
            Name = name;
        }

        [ConfigurationProperty("name", IsKey=true, IsRequired = true)]
        public String Name
        {
            get
            { return (String)this["name"]; }
            set
            { this["name"] = value; }
        }

        [ConfigurationProperty("units", IsDefaultCollection = false)]
        public UnitCollection Units
        {
            get { return (UnitCollection)base["units"]; }
        }
    }
}
