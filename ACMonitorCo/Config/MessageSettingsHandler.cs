﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public class MessageSettingsHandler : ConfigurationSection
    {
        public MessageSettingsHandler()
        {
        }

        public MessageSettingsHandler(String server)
        {
            Server = server;
        }

        [ConfigurationProperty("server", IsRequired = true)]
        public String Server
        {
            get { return (String)this["server"]; }
            set { this["server"] = value; }
        }

        [ConfigurationProperty("user", IsRequired = false)]
        public String User
        {
            get { return (String)this["user"]; }
            set { this["user"] = value; }
        }

        [ConfigurationProperty("pwd", IsRequired = false)]
        public String Password
        {
            get { return (String)this["pwd"]; }
            set { this["pwd"] = value; }
        }

        [ConfigurationProperty("port", IsRequired = false, DefaultValue=25)]
        public Int32 Port
        {
            get { return (Int32)this["port"]; }
            set { this["port"] = value; }
        }

        [ConfigurationProperty("from", IsRequired = false)]
        public String From
        {
            get { return (String)this["from"]; }
            set { this["from"] = value; }
        }

        [ConfigurationProperty("subject", IsRequired = false)]
        public String Subject
        {
            get { return (String)this["subject"]; }
            set { this["subject"] = value; }
        }
    }
}
