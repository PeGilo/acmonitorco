﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;

namespace ACMonitorCo
{
    /// <summary>
    /// Возвращает название счетчика по его номеру
    /// </summary>
    public class ShNameResolver : IShNameResolver
    {
        List<SHTXTItem> m_shtxtItems;

        public ShNameResolver(IList<SHTXTItem> shtxtItems)
        {
            if (shtxtItems != null)
            {
                // copy collection
                m_shtxtItems = new List<SHTXTItem>(shtxtItems);
            }
            else
            {
                throw new ArgumentException("Список <НомерСчетчика - НазваниеСчетчика> не должен быть равен NULL");
            }
        }

        /// <summary>
        /// Возвращает название счетчика
        /// </summary>
        /// <param name="nSh">Номер счетчика</param>
        /// <returns></returns>
        public string GetName(Int32 nSh)
        {
            SHTXTItem result = m_shtxtItems.Find(delegate(SHTXTItem item) { return item.N_SH == nSh; } );
            if (result != null)
                return result.TXT;
            else
                return String.Empty;
        }
    }
}
