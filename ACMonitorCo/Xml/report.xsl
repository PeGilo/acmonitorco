<?xml version="1.0" encoding="ISO-8859-1"?>
<xsl:transform version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
      <html>
        <head>
            <style type="text/css">
              body {font-size:12pt; font-family:Consolas;}

              .level0 { background: green; width: 15px;}
              .level1 { background: yellow; width: 15px;}
              .level2 { background: red; width: 15px;}

			  .regPointHeader {color: #2C36A0; font-size: 120%; font-weight: bold;}
				
              #eventTable {border: 1px solid black; border-collapse: collapse;}
              #eventTable td {border: 1px solid black; padding: 2px 5px 2px 5px; }
              #eventTable #header {text-align: center; background:#DBE5F1; padding: 3px 5px 3px 5px;}

              #dataTable {border: 1px solid black; border-collapse: collapse;}
              #dataTable td {border: 1px solid black; padding: 2px 5px 2px 5px; }
              #dataTable #header {text-align: center; background:#F2F2F2; padding: 3px 5px 3px 5px;}

            </style>
        </head>
        <body>
           <xsl:apply-templates/>   
        </body>
      </html>
    </xsl:template>
    
    <xsl:template match="acreport">
        <!-- ��������� -->
        <table style="width:100%;">
            <tr>
                <td style="text-align:center;">
                    <xsl:value-of select="@title"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="@date"/>
                </td>
            </tr>
        </table>
        <br/>
        <xsl:apply-templates select="registrationPoints"/>
    </xsl:template>         

	<xsl:template match="registrationPoints">
		<xsl:apply-templates select="registrationPoint" />
	</xsl:template>
    
	<xsl:template match="registrationPoint">
		<span class="regPointHeader">
			<xsl:value-of select="@title"/>
		</span>
		<p/>
		<xsl:apply-templates select="events" />
        <br/>
        <xsl:apply-templates select="data" />
		<br/>		
	</xsl:template>
	
    <!-- ������� ������� -->
    <xsl:template match="events">
    <table id="eventTable">
        <!-- ��������� -->
        <tr>
            <td id="header" colspan="3">
                <xsl:value-of select="@title" />
            </td>
        </tr>
        <xsl:choose>
            <xsl:when test="not(node())"> <!-- �������� �� ������ ������ -->
                <tr>
                    <td class="level0">
                    </td>
                    <td colspan="2">
                        <xsl:value-of select="@emptyMessage"/>
                    </td>
                </tr>
            </xsl:when>
            <xsl:otherwise>
                <!-- ������-������� -->
                <xsl:for-each select="event">
                    <xsl:variable name="cssclass" select="@level"/>
                    <tr>
                        <td class="level{$cssclass}">
                        </td>
                        <td>
                            <xsl:value-of select="date"/>
                        </td>
                        <td>
                            <xsl:value-of select="message"/>
                        </td>
                    </tr>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </table>
    </xsl:template>    
    
    <!-- ������� ������ -->
    <xsl:template match="data">
    <table id="dataTable">
         <tr>
            <td id="header" colspan="4">
                <xsl:value-of select="@title"/>
            </td>
         </tr>
         <xsl:choose>
            <xsl:when test="not(node())">
                <tr>
                    <td class="level0">
                    </td>
                    <td colspan="3">
                        <xsl:value-of select="@emptyMessage"/>
                    </td>
                </tr>  
            </xsl:when>
            <xsl:otherwise>
               <!-- ������-������ -->
               <xsl:for-each select="dataevent">
               <xsl:variable name="cssclass" select="@level"/>
                <tr>
                    <td class="level{$cssclass}">
                    </td>
                    <td>
                        <xsl:value-of select="date"/>
                    </td>
                    <td>
                        <xsl:value-of select="message"/>
                    </td>
                    <td>
                        <xsl:value-of select="period"/>
                    </td>
                </tr>
                </xsl:for-each>
            </xsl:otherwise>
        </xsl:choose>
    </table>
    </xsl:template>    

</xsl:transform>
