﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public enum Category
    {
        General,
        Trace,
        Database,
        Logic
    }
}
