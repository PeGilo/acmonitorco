﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public interface IShNameResolver
    {
        string GetName(Int32 nSh);
    }
}
