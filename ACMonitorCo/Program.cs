﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.OracleClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Xml.Xsl;
using System.Net;
using System.Net.Mail;

using Microsoft.Practices.EnterpriseLibrary.Common.Configuration;
using Microsoft.Practices.EnterpriseLibrary.Logging;
using Microsoft.Practices.EnterpriseLibrary.Logging.ExtraInformation;
using Microsoft.Practices.EnterpriseLibrary.Logging.Filters;
using Microsoft.Practices.EnterpriseLibrary.ExceptionHandling;

using ACMonitorCo.DataAccess;
using ACMonitorCo.Logic;

namespace ACMonitorCo
{
    public class Program
    {
        /// <summary>
        /// Добавляется к текущим суткам, чтобы захватывать события других суток (в минутах).
        /// </summary>
        const int DELTA_MINUNITES = 15;

        static void Main(string[] args)
        {
            ExceptionManager em = EnterpriseLibraryContainer.Current.GetInstance<ExceptionManager>();
            LogWriter logWriter = EnterpriseLibraryContainer.Current.GetInstance<LogWriter>();

            string reportDayStr = ConfigurationManager.AppSettings["ReportDay"];
            DateTime reportDay = DateTime.Now;
            em.Process(() => reportDay = ParseReportDay(reportDayStr),
                "UI Log&Exit");

            string connString = ConfigurationManager.ConnectionStrings["OracleConn"].ConnectionString;
            string xsltPath = ConfigurationManager.AppSettings["XsltPath"];
            string outputHtmlPath = ConfigurationManager.AppSettings["outputHtmlPath"];

            List<EVLOGItem> logItems = new List<EVLOGItem>();
            List<EVLOGRTUItem> logRtuItems = new List<EVLOGRTUItem>();
            List<SHTXTItem> shtxtItems = new List<SHTXTItem>();
            List<INTERVItem> intervItems = new List<INTERVItem>();

            // Read data from database
            em.Process(
                () => DBReadData(connString, ref logItems, ref logRtuItems, ref shtxtItems, ref intervItems, reportDay, em), 
                "UI Log&Exit");
            
            //Mock_DBReadData(connString, logItems, shtxtItems, intervItems, reportDay);

            logWriter.Write("Data has been read. Continue...", Category.Trace.ToString(), -1, -1, System.Diagnostics.TraceEventType.Information);

            RegistrationPointListHandler registrationPointSettings =
                (RegistrationPointListHandler)ConfigurationManager.GetSection("registrationPointList");

            XmlACReport report = null;
            Level reportLevel = Level.Green;

            em.Process(
                () => report = CreateReport(reportDay, logItems, logRtuItems, shtxtItems, intervItems, registrationPointSettings, out reportLevel),
                "UI Log&Exit");

            // Serialize report to xml
            Stream stream = null;
            em.Process(
                () => stream = SerializeReportToXml(report),
                "UI Log&Exit");
            
            // Write xslt transformation to html file
            em.Process(
                () => TransformXmlToFile(xsltPath, outputHtmlPath, stream),
                "UI Log&Continue");

            stream.Seek(0, SeekOrigin.Begin);
            // Write xslt transformation to string
            String reportContent = String.Empty;
            em.Process(
                () => reportContent = TransformXmlToString(xsltPath, stream),
                "UI Log&Exit");

            stream.Close();

            // Read configuration for mailing list
            MessageSettingsHandler messageSettings = null;
            MailingListHandler mailingList = null;

            em.Process(
                () =>
                {
                    messageSettings =
                      (MessageSettingsHandler)ConfigurationManager.GetSection(
                      "mailing/messageSettings");

                    mailingList =
                      (MailingListHandler)ConfigurationManager.GetSection(
                      "mailing/mailingList");
                },
                "UI Log&Exit");

            // Mail report to recipients
            em.Process(
                () => MailReport(reportContent, reportLevel, messageSettings, mailingList.Recipients, em, logWriter),
                "UI Log&Exit");


        }

        /// <summary>
        /// Parses parameter string that specifies the day of report.
        /// The string may be a number. In this case it means a number of days ago. Ex., "2".
        /// Or the string may be a date. Ex., "19.02.2010".
        /// </summary>
        /// <param name="reportDayStr"></param>
        /// <returns>Date of report</returns>
        public static DateTime ParseReportDay(string reportDayStr)
        {
            Int32 days;
            if (Int32.TryParse(reportDayStr, out days))
            {
                return DateTime.Now.AddDays(-Math.Abs(days));
            }
            DateTime date;
            if(DateTime.TryParse(reportDayStr, out date))
            {
                return date;
            }
            throw new InvalidOperationException("Can't parse report day string");
        }

        private static XmlACReport CreateReport(DateTime curDate,
            List<EVLOGItem> logItems, List<EVLOGRTUItem> logRtuItems, List<SHTXTItem> shtxtItems, List<INTERVItem> intervItems,
            RegistrationPointListHandler regPointList,
            out Level reportLevel)
        {
            // Sort data by time
            logItems.Sort(new Comparison<EVLOGItem>(delegate(EVLOGItem x, EVLOGItem y) 
                { return (int)((x.EVTIME - y.EVTIME).TotalMilliseconds); }));

            // Parse data to logic events
            IEnumerable<CounterEvent> logCounterEvents = DataParser.Parse(logItems);
            IEnumerable<RtuEvent> logRtuEvents = DataParser.Parse(logRtuItems);
            IEnumerable<CounterEvent> intervEvents = DataParser.Parse(intervItems);

            
            //logEvents = from e in logEvents
            //            where e.Time.Day == curDate.Day
            //            select e;

            // Convert events to xml-compliant events
            // and filter out events by current date
            IShNameResolver nameResolver = new ShNameResolver(shtxtItems);
            List<XmlEvent> xmlEvents = new List<XmlEvent>();

            xmlEvents.AddRange(from le in logCounterEvents
                               where le.Time.Day == curDate.Day
                               orderby le.Time
                               select XmlEventsFactory.CreateCounterEvent(le, nameResolver));

            xmlEvents.AddRange(from le in logRtuEvents
                               where le.Time.Day == curDate.Day
                               orderby le.Time
                               select XmlEventsFactory.CreateRtuEvent(le));

            // Sort events by date and message
            xmlEvents.Sort(new Comparison<XmlEvent>(
                (XmlEvent lhs, XmlEvent rhs) => {
                    if (lhs.Date == rhs.Date)
                        return lhs.Message.CompareTo(rhs.Message);
                    else
                        return lhs.Date.CompareTo(rhs.Date);
                }));

            IEnumerable<XmlDataEvent> xmlDataEvents = from ie in intervEvents
                                                      orderby ie.Time
                                                      select XmlEventsFactory.CreateDataEvent(ie, nameResolver);
            // Sort events by message
            xmlDataEvents = from xde in xmlDataEvents
                            orderby xde.Date, xde.Message, xde.Period
                            select xde;

            // Divide by Registration points
            //XmlRegisrationPoint

            // Create report
            XmlACReport report = CreateXmlReport(curDate, xmlEvents, xmlDataEvents, regPointList);

            // Calculate Level of report - it is the maximum level of events
            reportLevel = Level.Green; // default
            foreach (XmlEvent xe in xmlEvents)
            {
                if (xe.Level > reportLevel)
                    reportLevel = xe.Level;
            }
            foreach (XmlDataEvent xde in xmlDataEvents)
            {
                if (xde.Level > reportLevel)
                    reportLevel = xde.Level;
            }
            return report;
        }

        #region "Mailing"

        /// <summary>
        /// Рассылает отчет всем получателям из конфигурационного списка
        /// </summary>
        /// <param name="reportContent">Содержимое отчета в формат HTML</param>
        /// <param name="reportLevel">Уровень отчета.
        /// Green - содержит только сообщения уровня Green.
        /// Yellow - содержит сообщения уровня Green и Yellow.
        /// Red - содержит сообщения всех уровней, включая Red.</param>
        public static void MailReport(string reportContent, Level reportLevel, 
            MessageSettingsHandler messageSettings,
            RecipientCollection recipients,
            ExceptionManager em,
            LogWriter logWriter)
        {
            // Mailing to all recipients
            MailAddress from = new MailAddress(messageSettings.From);
            SmtpClient client = new SmtpClient(messageSettings.Server, messageSettings.Port);

            foreach (RecipientElement recip in recipients)
            {
                // Посылать отчет только тем у кого затребованный уровень 
                // включает в себя уровень текущего отчета
                if (recip.Level <= reportLevel)
                {
                    MailAddress to = new MailAddress(recip.Email);
                    MailMessage msg = new MailMessage(from, to);
                    msg.Subject = messageSettings.Subject;
                    msg.IsBodyHtml = true;
                    msg.Body = reportContent;

                    // Чтобы аутентификация использовала не GSSAPI метод, а простой LOGIN, надо
                    // использоваться явные Credentials (User name, Password, Domain)
                    if (!String.IsNullOrEmpty(messageSettings.User))
                    {
                        string[] credentials = messageSettings.User.Split(new char[] { '\\' }, StringSplitOptions.RemoveEmptyEntries);
                        if (credentials.Length == 2)
                        {
                            client.UseDefaultCredentials = false;
                            client.Credentials = new NetworkCredential(credentials[1], messageSettings.Password, credentials[0]);
                        }
                    }

                    logWriter.Write("Sending report to " + recip.Email, Category.Trace.ToString(), -1, -1, System.Diagnostics.TraceEventType.Information);
                    em.Process(() => client.Send(msg), "UI Log&Continue");
                }
            }
            logWriter.Write("All reports have been sent", Category.Trace.ToString(), -1, -1, System.Diagnostics.TraceEventType.Information);
        }

        #endregion

        #region "XML Report"

        /// <summary>
        ///Трансформирует XML в HTML при помощи XSLT и записывает в файл
        /// </summary>
        /// <param name="xsltPath">Путь к существующему файлу *.xsl</param>
        /// <param name="outputHtmlPath">Путь к создаваемому файлу HTML</param>
        /// <param name="stream">Поток, в котором записан XML</param>
        private static void TransformXmlToFile(string xsltPath, string outputHtmlPath, Stream stream)
        {
            XmlReader xmlReader = new XmlTextReader(stream);
            XmlWriter htmlWriter = new XmlTextWriter(outputHtmlPath, Encoding.UTF8);
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(xsltPath);
            xslt.Transform(xmlReader, htmlWriter);

            //xmlReader.Close();
            htmlWriter.Close();
        }

        private static string TransformXmlToString(string xsltPath, Stream stream)
        {
            XmlReader xmlReader = new XmlTextReader(stream);
            XslCompiledTransform xslt = new XslCompiledTransform();
            xslt.Load(xsltPath);

            StringBuilder sb = new StringBuilder();
            StringWriter sw = new StringWriter(sb);
            xslt.Transform(xmlReader, new XsltArgumentList(), sw);

            //xmlReader.Close();
            sw.Close();
            
            return sb.ToString();
        }

        /// <summary>
        /// Сериализует отчет в поток в виде XML
        /// </summary>
        /// <param name="report">Объект-отчет, готовый к сериализации</param>
        /// <returns>Поток, в который записан сгенерированный XML</returns>
        private static Stream SerializeReportToXml(XmlACReport report)
        {
            Stream stream = new MemoryStream();
            XmlSerializer ser = new XmlSerializer(typeof(XmlACReport));
            //try
            //{
                ser.Serialize(stream, report);
            //}
            //catch (InvalidOperationException ex) {
            //    // An error occurred during serialization. 
            //    // The original exception is available using the InnerException property.
            //}

            stream.Seek(0, SeekOrigin.Begin);
            return stream;
        }

        /// <summary>
        /// Конструирует отчет из списка событий и списка интервалов опроса, готовый к сериализации
        /// </summary>
        /// <param name="curDate">Дата, которая будет проставлена в отчете</param>
        /// <param name="xmlEvents">Список событий, который можно сериализовать</param>
        /// <param name="xmlDataEvents">Список интервалов, который можно сериализовать</param>
        /// <returns>Объект отчет, готовый к сериализации</returns>
        private static XmlACReport CreateXmlReport(DateTime curDate, 
            IEnumerable<XmlEvent> xmlEvents, IEnumerable<XmlDataEvent> xmlDataEvents,
            RegistrationPointListHandler regPointList)
        {
            XmlACReport report;
            List<XmlRegistrationPoint> xmlRegPoints = new List<XmlRegistrationPoint>();

            foreach (RegistrationPointElement regPoint in regPointList.RegistrationPoints)
            {
                // Filter events and data events
                XmlEvent[] events = (from e in xmlEvents 
                                     where regPoint.Units.ContainsCounterNumber(e.UnitNumber) 
                                     select e).ToArray();
                XmlDataEvent[] dataEvents = (from de in xmlDataEvents
                                             where regPoint.Units.ContainsCounterNumber(de.CounterNumber)
                                             select de).ToArray();

                XmlRegistrationPoint xmlRegPoint = new XmlRegistrationPoint(events, dataEvents);
                xmlRegPoint.Title = regPoint.Name;
                xmlRegPoint.Events.Title = GlobalResource.ReportEventsTitle;
                xmlRegPoint.Events.EmptyMessage = GlobalResource.ReportEventsEmptyMessage;

                xmlRegPoint.DataEvents.Title = GlobalResource.ReportDataTitle;
                xmlRegPoint.DataEvents.EmptyMessage = GlobalResource.ReportDataEmptyMessage;

                xmlRegPoints.Add(xmlRegPoint);
            }

            report = new XmlACReport(xmlRegPoints.ToArray());
            
            report.Title = GlobalResource.ReportMainTitle;
            report.Date = curDate.ToString("dd MMM yyyy"); //"2010-05-28";

            return report;
        }

        #endregion

        #region "Database Reading"

        /// <summary>
        /// Читает данные (события счетчиков, данные по интервалам) из базы данных
        /// и заполняет ими списки, которые передаются как параметры
        /// </summary>
        /// <param name="connString">Строка соединения с базой</param>
        /// <param name="items">Строки таблицы событий</param>
        /// <param name="shtxt_items">Строки таблицы НомерСчетчика-Название</param>
        /// <param name="interv_items">Строки таблицы ошибочных интервалов опроса</param>
        /// <param name="day">День за который необходимо считать данные</param>
        private static void DBReadData(string connString,
            ref List<EVLOGItem> items, ref List<EVLOGRTUItem> rtuItems, 
            ref List<SHTXTItem> shtxt_items, ref List<INTERVItem> interv_items,
            DateTime day, ExceptionManager em)
        {
            using (OracleConnection conn = new OracleConnection(connString))
            {
                try
                {
                    conn.Open();
                    // Берем сутки +- 15 минут, чтобы не осталось "хвостов" от других суток,
                    // и чтобы они не засчитались как события расчетных суток.
                    DateTime logDateStart = (new DateTime(day.Year, day.Month, day.Day)).AddMinutes(-DELTA_MINUNITES);
                    DateTime logDateEnd = (new DateTime(day.Year, day.Month, day.Day)).AddDays(1).AddMinutes(DELTA_MINUNITES);

                    items = DB.ReadEVLOG(conn, logDateStart, logDateEnd);
                    rtuItems = DB.ReadEVLOGRTU(conn, logDateStart, logDateEnd);
                    shtxt_items = DB.ReadSHTXT(conn);
                    interv_items = DB.ReadBadIntervals(conn, day);
                }
                catch (InvalidOperationException ex) //The connection is not open. 
                {
                    em.HandleException(ex, "DA Log&Wrap Conn");
                }
                catch (OracleException ex) // Cannot change the database or other exception
                {
                    em.HandleException(ex, "DA Log&Wrap Read");
                }
            }
        }

        /// <summary>
        /// Замена метода, который читает данные из базы (для тестирования)
        /// </summary>
        /// <param name="connString"></param>
        /// <param name="items"></param>
        /// <param name="shtxt_items"></param>
        /// <param name="interv_items"></param>
        /// <param name="day"></param>
        private static void Mock_DBReadData(string connString,
            List<EVLOGItem> items, List<SHTXTItem> shtxt_items, List<INTERVItem> interv_items,
            DateTime day)
        {
            // Кратковременное включение
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 208));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 208));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 209));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 210));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 01, 11), 212));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 210));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 211));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 213));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 16, 47, 00, 11), 212));
            // Кратковременное включение
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 208));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 209));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 210));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 208));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 15, 11), 212));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 210));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 212));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 213));
            items.Add(new EVLOGItem(1155843, new DateTime(2010, 02, 19, 15, 37, 13, 11), 211));
            // Отключение
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 206));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 208));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 210));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 210));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 20, 09, 11), 212));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 208));
            items.Add(new EVLOGItem(1155870, new DateTime(2010, 02, 19, 00, 15, 02, 11), 212));

            interv_items.Add(new INTERVItem(new DateTime(2010, 02, 19, 13, 45, 00), 1155865, 26, 750, 780));

            shtxt_items.Add(new SHTXTItem(1155843, "ЭК43"));
            shtxt_items.Add(new SHTXTItem(1155870, "ЭК70"));
            shtxt_items.Add(new SHTXTItem(1155865, "ЭК65"));
        }
        #endregion

        static void HandleException(Exception ex, bool exit)
        {
            Console.WriteLine(ex.Message);
            if (exit)
            {
                // exit with error
                Environment.Exit(1);
            }
        }
    }
}
