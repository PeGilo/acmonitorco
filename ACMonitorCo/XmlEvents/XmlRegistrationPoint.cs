﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    [XmlType(TypeName = "registrationPoint")]
    public class XmlRegistrationPoint
    {
        [XmlAttribute(AttributeName="title")]
        public string Title { get; set; }

        public XmlRegistrationPoint()
        {
        }

        public XmlRegistrationPoint(XmlEvent[] ev, XmlDataEvent[] dev)
        {
            Events = new XmlEvents(ev);
            DataEvents = new XmlDataEvents(dev);
        }

        [XmlElement("events")]
        public XmlEvents Events { get; set; }

        [XmlElement("data")]
        public XmlDataEvents DataEvents { get; set; }
    }
}
