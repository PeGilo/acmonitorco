﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    [XmlType(TypeName = "registrationPoints")]
    public class XmlRegistrationPoints
    {
        public XmlRegistrationPoints()
        {
            Points = new XmlRegistrationPoint[0];
        }

        public XmlRegistrationPoints(XmlRegistrationPoint[] points)
        {
            Points = points;
        }

        [XmlElement("registrationPoint")]
        public XmlRegistrationPoint[] Points { get; set; }
    }
}
