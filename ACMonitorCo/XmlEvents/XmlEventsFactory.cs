﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.Logic;

namespace ACMonitorCo
{
    /// <summary>
    /// Creates events structures that can be serialized to xml
    /// from events objects from logic model.
    /// </summary>
    /// <remarks>Пока что совмещает две функции - выбор сообщения по типу события и фабрику xml событий</remarks>
    public static class XmlEventsFactory
    {
        private const string XMLEVENT_DATEFORMAT = "dd MMMM yyyy HH:mm";
        private const string XMLDATAEVENT_DATEFORMAT = "dd MMMM yyyy";

        public static XmlEvent CreateCounterEvent(CounterEvent ev, IShNameResolver nameResolver)
        {
            string message = String.Empty;
            string date = ev.Time.ToString(XMLEVENT_DATEFORMAT);
            string eventSource = nameResolver.GetName(ev.N_SH);
            Level level = Level.Green;

            if (ev is SurgeEvent)
            {
                message = GlobalResource.MessageSurgeEvent;
                message += " " + eventSource;
                level = Level.Yellow;
            }
            else if (ev is SwitchingEvent)
            {
                SwitchingEvent se = (SwitchingEvent)ev;
                if (se.Operation == Switching.On)
                {
                    message = GlobalResource.MessageSwitchingOnEvent;
                }
                else
                {
                    message = GlobalResource.MessageSwitchingOffEvent;
                }
                message += " " + eventSource;
            }
            else if (ev is PhaseFailureEvent)
            {
                message = GlobalResource.MessagePhaseFailureEvent;
                message += " " + eventSource;
                level = Level.Red;
            }
            else if (ev is PowerFailureEvent)
            {
                PowerFailureEvent pfe = (PowerFailureEvent)ev;
                if (pfe.Operation == Switching.On)
                {
                    message = GlobalResource.MessagePowerOnEvent;
                }
                else
                {
                    message = GlobalResource.MessagePowerOffEvent;
                }
                message += " " + eventSource;
                level = Level.Red;
            }
            else if (ev is CounterTimeCorrectionEvent)
            {
                CounterTimeCorrectionEvent ctce = (CounterTimeCorrectionEvent)ev;
                message = GlobalResource.MessageCounterTimeCorrectionEvent
                    + " " + eventSource
                    + " (" + ctce.CorrectionOffset.TotalSeconds.ToString() + " " + GlobalResource.TimeCorrectionTicks + ")";
            }
            else
            {
                throw new ArgumentException();
            }

            return new XmlEvent(date, message, ev.N_SH, level);
        }

        public static XmlEvent CreateRtuEvent(RtuEvent ev)
        {
            string message = String.Empty;
            string date = ev.Time.ToString(XMLEVENT_DATEFORMAT);
            Level level = Level.Green;

            if (ev is RtuTimeCorrectionEvent)
            {
                message = GlobalResource.MessageRtuTimeCorrectionEvent 
                    + " " + ((RtuTimeCorrectionEvent)ev).CorrectionOffset;
            }
            else
            {
                throw new ArgumentException();
            }

            return new XmlEvent(date, message, ev.RtuNumber, level);
        }

        public static XmlDataEvent CreateDataEvent(CounterEvent ev, IShNameResolver nameResolver)
        {
            string message = String.Empty;
            string date = ev.Time.ToString(XMLDATAEVENT_DATEFORMAT);
            string period = String.Empty;
            Level level = Level.Red;

            if (ev is InvalidReadingEvent)
            {
                InvalidReadingEvent ire = (InvalidReadingEvent)ev;
                period = String.Format("{0} - {1}", ire.Time.ToString("HH:mm"),
                    ire.Time.AddMinutes(ire.Interval).ToString("HH:mm"));
                message = GlobalResource.MessageInvalidReadingEvent;
            }
            else
            {
                throw new ArgumentException();
            }

            // Добавить название источника события
            message += " " + nameResolver.GetName(ev.N_SH);

            return new XmlDataEvent(date, message, period, ev.N_SH, level);
        }
    }
}
