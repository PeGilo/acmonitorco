﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    [XmlType(TypeName = "events")]
    public class XmlEvents
    {
        [XmlAttribute(AttributeName = "title")]
        public string Title { get; set; }
        [XmlAttribute(AttributeName = "emptyMessage")]
        public string EmptyMessage { get; set; }

        public XmlEvents()
        {
            Events = new XmlEvent[0];
        }

        public XmlEvents(XmlEvent[] ev)
        {
            Events = ev;
        }

        [XmlElement("event")]
        public XmlEvent[] Events { get; set; }
    }
}
