﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo
{
    public abstract class XmlDataEventBase
    {
        public abstract string Date { get; }
        public abstract string Message { get; }
        public abstract string Period { get; }
    }
}
