﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    [XmlType(TypeName = "data")]
    public class XmlDataEvents
    {
        [XmlAttribute(AttributeName = "title")]
        public string Title { get; set; }
        [XmlAttribute(AttributeName = "emptyMessage")]
        public string EmptyMessage { get; set; }

        public XmlDataEvents()
        {
            DataEvents = new XmlDataEvent[0];
        }
        
        public XmlDataEvents(XmlDataEvent[] dev)
        {
            DataEvents = dev;
        }
        
        [XmlElement("dataevent")]
        public XmlDataEvent[] DataEvents { get; set; }
    }
}
