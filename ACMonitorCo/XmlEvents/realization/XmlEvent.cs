﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    //[Serializable]
    [XmlType(TypeName="event")]
    public class XmlEvent //: XmlEventBase
    {
        public XmlEvent()
        {
            Date = String.Empty;
            Message = String.Empty;
        }

        public XmlEvent(string date, string message, int unitNumber)
        {
            Date = date;
            Message = message;
            UnitNumber = unitNumber;
            Level = Level.Green;
        }

        public XmlEvent(string date, string message, int unitNumber, Level level)
            : this(date, message, unitNumber)
        {
            Level = level;
        }

        [XmlElement(ElementName="date")]
        public virtual string Date { get; set; }
        [XmlElement(ElementName = "message")]
        public virtual string Message { get; set; }
        [XmlAttribute(AttributeName = "level")]
        public virtual Level Level { get; set; }

        [XmlIgnore]
        public virtual Int32 UnitNumber { get; set; }
    }
}
