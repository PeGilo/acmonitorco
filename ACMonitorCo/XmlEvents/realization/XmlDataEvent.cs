﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    //[Serializable]
    [XmlType(TypeName = "dataevent")]
    public class XmlDataEvent //: XmlDataEventBase
    {
        public XmlDataEvent()
        {
            Date = String.Empty;
            Message = String.Empty;
            Period = String.Empty;
        }

        public XmlDataEvent(string date, string message, string period, int counterNumber)
        {
            Date = date;
            Message = message;
            Period = period;
            CounterNumber = counterNumber;
            Level = Level.Green;
        }

        public XmlDataEvent(string date, string message, string period, int counterNumber, Level level)
            : this(date, message, period, counterNumber)
        {
            Level = level;
        }

        [XmlElement(ElementName = "date")]
        public virtual string Date { get; set; }
        [XmlElement(ElementName = "message")]
        public virtual string Message { get; set; }
        [XmlElement(ElementName = "period")]
        public virtual string Period { get; set; }
        [XmlAttribute(AttributeName = "level")]
        public virtual Level Level { get; set; }

        [XmlIgnore]
        public virtual Int32 CounterNumber { get; set; }
    }
}
