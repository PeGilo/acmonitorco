﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    /// <summary>
    /// Уровень серьезности события
    /// </summary>
    public enum Level
    {
        [XmlEnum(Name = "0")]
        Green = 0,
        [XmlEnum(Name = "1")]
        Yellow = 1,
        [XmlEnum(Name = "2")]
        Red = 2
    }
}
