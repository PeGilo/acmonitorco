﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo
{
    [XmlType(TypeName = "acreport")]
    public class XmlACReport
    {
        [XmlAttribute(AttributeName="title")]
        public string Title { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string Date { get; set; }

        public XmlACReport()
        {
        }

        public XmlACReport(XmlRegistrationPoint[] points)
        {
            Points = new XmlRegistrationPoints(points);
        }

        [XmlElement("registrationPoints")]
        public XmlRegistrationPoints Points { get; set; }
    }
}
