﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Utils.Test
{
    /// <summary>
    /// Summary description for UnitTest1
    /// </summary>
    [TestClass]
    public class ListUtils
    {
        class SomeClass
        {
            public int aNumber;
            public string aString;

            public SomeClass(int n, string s)
            {
                aNumber = n;
                aString = s;
            }
        }

        public ListUtils()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        private TestContext testContextInstance;

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }

        #region Additional test attributes
        //
        // You can use the following additional attributes as you write your tests:
        //
        // Use ClassInitialize to run code before running the first test in the class
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Use ClassCleanup to run code after all tests in a class have run
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Use TestInitialize to run code before running each test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Use TestCleanup to run code after each test has run
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestRemoveItemsOnInteger()
        {
            LinkedList<int> list = new LinkedList<int>();
            list.AddFirst(4);
            list.AddFirst(3);
            list.AddFirst(2);
            list.AddFirst(1);

            Assert.AreEqual(4, list.Count);

            list.RemoveItems<int>(new int[] { 1, 2 });
            Assert.AreEqual(2, list.Count);

            list.RemoveItems<int>(new int[] { 3, 4 });
            Assert.AreEqual(0, list.Count);
        }

        [TestMethod]
        public void TestRemoveItemsOnStructures()
        {
            LinkedList<SomeClass> list = new LinkedList<SomeClass>();
            list.AddFirst(new SomeClass(1, "1"));
            list.AddFirst(new SomeClass(2, "2"));
            list.AddFirst(new SomeClass(3, "3"));
            list.AddFirst(new SomeClass(4, "4"));

            Assert.AreEqual(4, list.Count);

            IEnumerable<SomeClass> rem = from l in list where l.aNumber == 1 || l.aNumber == 2 select l;
            list.RemoveItems<SomeClass>(rem);
            Assert.AreEqual(2, list.Count);

            rem = from l in list where l.aNumber == 3 || l.aNumber == 4 select l;
            list.RemoveItems<SomeClass>(rem);
            Assert.AreEqual(0, list.Count);
        }
    }
}
