﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess.Exceptions
{
    public class DBConnectionException : Exception
    {
        public DBConnectionException()
            : base()
        {
        }

        public DBConnectionException(string message)
            : base(message)
        {
        }

        public DBConnectionException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
