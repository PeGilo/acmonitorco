﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess.Exceptions
{
    public class DBReadingException : Exception
    {
        public DBReadingException()
            : base()
        {
        }

        public DBReadingException(string message)
            : base(message)
        {
        }

        public DBReadingException(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
