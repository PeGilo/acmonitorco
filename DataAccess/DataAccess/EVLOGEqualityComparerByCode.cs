﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    internal class EVLOGEqualityComparerByCode : IEqualityComparer<EVLOGItem>
    {
        public bool Equals(EVLOGItem x, EVLOGItem y)
        {
            return x.EVTYPE == y.EVTYPE;
        }

        public int GetHashCode(EVLOGItem x)
        {
            return x.EVTYPE.GetHashCode();
        }
    }
}
