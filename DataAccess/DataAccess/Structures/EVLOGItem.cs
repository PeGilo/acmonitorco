﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    public class EVLOGItem
    {
        public Int32 N_SH { get; set; }
        public DateTime EVTIME { get; set; }
        public Int32 EVTYPE { get; set; }

        public EVLOGItem(Int32 n_sh, DateTime evtime, Int32 evtype)
        {
            N_SH = n_sh;
            EVTIME = evtime;
            EVTYPE = evtype;
        }

        public override string ToString()
        {
            return N_SH.ToString() + ", " + EVTIME.ToString() + ", " + EVTYPE.ToString();
        }

        public static IEqualityComparer<EVLOGItem> GetEqualityComparerByCode()
        {
            return new EVLOGEqualityComparerByCode();
        }
    }
}
