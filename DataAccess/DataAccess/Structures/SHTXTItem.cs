﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    public class SHTXTItem
    {
        public Int32 N_SH { get; set; }
        public String TXT { get; set; }

        public SHTXTItem(Int32 n_sh, String txt)
        {
            N_SH = n_sh;
            TXT = txt;
        }

        public override string ToString()
        {
            return N_SH.ToString() + ", " + TXT;
        }
    }
}
