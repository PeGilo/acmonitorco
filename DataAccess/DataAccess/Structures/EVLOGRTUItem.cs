﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    public class EVLOGRTUItem
    {
        public DateTime EVTIME { get; set; }
        public Int32 EVTYPE { get; set; }
        public Int32 EVCODE { get; set; }
        public String TXT { get; set; }

        public EVLOGRTUItem(DateTime evtime, Int32 evtype, Int32 evcode, String txt)
        {
            EVTIME = evtime;
            EVTYPE = evtype;
            EVCODE = evcode;
            TXT = txt;
        }

        public override string ToString()
        {
            return EVTIME.ToString() + ", " + EVTYPE.ToString() + ", " + EVCODE.ToString() + ", " + TXT;
        }
    }
}
