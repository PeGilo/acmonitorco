﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    public class INTERVItem
    {
        public DateTime DD_MM_YYYY { get; set; }
        public Int32 N_SH { get; set; }
        public Int32 N_INTERV_RAS { get; set; }
        public Int32 MIN_0 { get; set; }
        public Int32 MIN_1 { get; set; }

        public INTERVItem(DateTime date, int n_sh, int n_interv_ras, int min_0, int min_1)
        {
            DD_MM_YYYY = date;
            N_SH = n_sh;
            N_INTERV_RAS = n_interv_ras;
            MIN_0 = min_0;
            MIN_1 = min_1;
        }

        public override string ToString()
        {
            return DD_MM_YYYY.ToString() + ", " + N_SH + ", " + N_INTERV_RAS + ", " + MIN_0 + ", " + MIN_1;
        }
    }
}
