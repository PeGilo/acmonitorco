﻿using System;
using System.Collections.Generic;
using System.Data.OracleClient;
using System.Linq;
using System.Text;

namespace ACMonitorCo.DataAccess
{
    /// <summary>
    /// Contains methods for reading data from oracle database
    /// </summary>
    public static class DB
    {
        private const int SYB_RNK = 3;
        //private const int N_OB = 2;
        private static int[] n_ob = new int[] {2, 3};

        public static List<EVLOGItem> ReadEVLOG(OracleConnection conn, DateTime start, DateTime end)
        {
            OracleCommand command =
                new OracleCommand("select N_SH, EVTIME, EVTYPE from cnt.EV_LOG where"
                                + " EVTIME > to_date('" + start.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')"
                                + " and EVTIME < to_date('" + end.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')");

            command.Connection = conn;

            List<EVLOGItem> evlogItems = new List<EVLOGItem>();

            OracleDataReader dr = command.ExecuteReader();
            try
            {
                // Always call Read before accessing data.
                while (dr.Read())
                {
                    evlogItems.Add(new EVLOGItem(dr.GetInt32(0), dr.GetDateTime(1), dr.GetInt32(2)));
                }
            }
            finally
            {
                // Always call Close when done reading.
                dr.Close();
            }

            return evlogItems;
        }

        public static List<EVLOGRTUItem> ReadEVLOGRTU(OracleConnection conn, DateTime start, DateTime end)
        {
            OracleCommand command =
                new OracleCommand("select EVTIME, EVTYPE, EVCODE, TXT from cnt.EVLOG_RTU where"
                                + " EVTIME > to_date('" + start.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')"
                                + " and EVTIME < to_date('" + end.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')"
                                );

            command.Connection = conn;

            List<EVLOGRTUItem> evlogItems = new List<EVLOGRTUItem>();

            OracleDataReader dr = command.ExecuteReader();
            try
            {
                // Always call Read before accessing data.
                while (dr.Read())
                {
                    evlogItems.Add(new EVLOGRTUItem(dr.GetDateTime(0), dr.GetInt32(1), dr.GetInt32(2), dr.GetString(3)));
                }
            }
            finally
            {
                // Always call Close when done reading.
                dr.Close();
            }

            return evlogItems;
        }

        //select sh.n_sh, fid.txt from sh join fid on sh.n_ob = fid.n_ob and sh.syb_rnk = fid.syb_rnk and sh.n_fid = fid.n_fid
        public static List<SHTXTItem> ReadSHTXT(OracleConnection conn)
        {
            OracleCommand command =
                new OracleCommand("select s.n_sh, f.txt "
                                  + "from cnt.sh s join cnt.fid f "
                                  + "on s.n_ob = f.n_ob and s.syb_rnk = f.syb_rnk and s.n_fid = f.n_fid");

            command.Connection = conn;

            List<SHTXTItem> shtxtItems = new List<SHTXTItem>();

            OracleDataReader dr = command.ExecuteReader();
            try
            {
                // Always call Read before accessing data.
                while (dr.Read())
                {
                    shtxtItems.Add(new SHTXTItem(dr.GetInt32(0), dr.GetString(1)));
                }
            }
            finally
            {
                // Always call Close when done reading.
                dr.Close();
            }

            return shtxtItems;
        }

        public static List<INTERVItem> ReadBadIntervals(OracleConnection conn, DateTime date)
        {
            OracleCommand command =
                new OracleCommand("select distinct dd_mm_yyyy, n_sh, n_inter_ras, min_0, min_1 "
                                  + "from cnt.buf_v_int where "
                                  + "syb_rnk = " + SYB_RNK.ToString() + " and n_ob in " + ListToString(n_ob) + " and "
                                  + "dd_mm_yyyy = TRUNC(to_date('" + date.ToString("yyyyMMddHHmmss") + "', 'YYYYMMDDHH24MISS')) "
                                  + "and (kol_db <> kol or (stat <> 0 and stat <> 1))");

            command.Connection = conn;

            List<INTERVItem> intervItems = new List<INTERVItem>();

            OracleDataReader dr = command.ExecuteReader();
            try
            {
                // Always call Read before accessing data.
                while (dr.Read())
                {
                    intervItems.Add(new INTERVItem(dr.GetDateTime(0), dr.GetInt32(1), dr.GetInt32(2), dr.GetInt32(3), dr.GetInt32(4)));
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                // Always call Close when done reading.
                dr.Close();
            }

            return intervItems;
        }

        private static string ListToString(int[] list)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("(");
            for (int i = 0; i < list.Length - 1; i++)
            {
                sb.Append(list[i].ToString() + ",");
            }
            if (list.Length - 1 >= 0)
            {
                sb.Append(list[list.Length - 1].ToString());
            }
            sb.Append(")");
            return sb.ToString();
        }
    }
}
