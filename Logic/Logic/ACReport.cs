﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Не используется
    /// </summary>
    public class ACReport
    {
        private List<Event> m_logEvents;
        private List<Event> m_intervEvents;

        public string Title { get; set; }
        public string Date { get; set; }

        public ACReport()
        {
            m_logEvents = new List<Event>();
            m_intervEvents = new List<Event>();
        }

        public ACReport(IEnumerable<Event> logEvents, IEnumerable<Event> intervEvents)
        {
            m_logEvents = new List<Event>(logEvents);
            m_intervEvents = new List<Event>(intervEvents);
        }

        public IEnumerable<Event> Events { get { return m_logEvents; } }

        public IEnumerable<Event> DataEvents { get { return m_intervEvents; } }
    }
}
