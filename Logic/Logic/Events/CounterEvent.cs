﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class CounterEvent : Event
    {
        public Int32 N_SH { get; protected set; }

        public CounterEvent(DateTime time, Int32 n_sh)
            : base(time)
        {
            N_SH = n_sh;
        }
    }
}
