﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class Event
    {
        public DateTime Time { get; protected set; }

        public Event(DateTime time)
        {
            Time = time;
        }
    }
}
