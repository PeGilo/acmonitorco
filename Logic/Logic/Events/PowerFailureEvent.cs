﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class PowerFailureEvent : CounterEvent
    {
        public Switching Operation
        {
            get;
            protected set;
        }

        public PowerFailureEvent(DateTime time, Int32 n_sh, Switching op)
            : base(time, n_sh)
        {
            Operation = op;
        }
    }
}
