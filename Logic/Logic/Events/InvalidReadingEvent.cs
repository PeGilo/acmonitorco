﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class InvalidReadingEvent : CounterEvent
    {
        /// <summary>
        /// In minutes. Value of interval from start-time.
        /// </summary>
        public Int32 Interval { get; protected set; }

        public InvalidReadingEvent(DateTime time, Int32 n_sh, int interval)
            : base(time, n_sh)
        {
            Interval = interval;
        }
    }
}
