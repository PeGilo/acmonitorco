﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class PhaseFailureEvent : CounterEvent
    {
        public PhaseFailureEvent(DateTime time, Int32 n_sh)
            : base(time, n_sh)
        {

        }
    }
}
