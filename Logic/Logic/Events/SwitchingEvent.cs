﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public enum Switching
    {
        Off,
        On
    }

    public class SwitchingEvent : CounterEvent
    {
        public Switching Operation { get; protected set; }

        public SwitchingEvent(DateTime time, Int32 n_sh, Switching operation)
            :base(time, n_sh)
        {
            Operation = operation;
        }
    }
}
