﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Событие коррекции времени в RTU
    /// </summary>
    public class RtuTimeCorrectionEvent : RtuEvent
    {
        /// <summary>
        /// Установленное время
        /// </summary>
        public DateTime NewTime { get; protected set; }

        /// <summary>
        /// На сколько изменилось время в секундах
        /// </summary>
        public string CorrectionOffset { get; protected set; }

        public RtuTimeCorrectionEvent(DateTime time, Int32 rtuNumber, String correctionOffset)
            :base(time, rtuNumber)
        {
            CorrectionOffset = correctionOffset;
        }

        //public RtuTimeCorrectionEvent(DateTime time, Int32 rtuNumber, DateTime newTime, int correctionOffset)
        //    : base(time, rtuNumber)
        //{
        //    NewTime = newTime;
        //    CorrectionOffset = correctionOffset;
        //}
    }
}
