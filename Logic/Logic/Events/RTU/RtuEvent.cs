﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    public class RtuEvent : Event
    {
        public Int32 RtuNumber { get; protected set; }

        public RtuEvent(DateTime time, Int32 rtuNumber)
            : base(time)
        {
            RtuNumber = rtuNumber;
        }
    }
}
