﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    /// <summary>
    /// Событие коррекции времени на счетчике
    /// </summary>
    public class CounterTimeCorrectionEvent : CounterEvent
    {
        /// <summary>
        /// Установленное время
        /// </summary>
        public DateTime NewTime { get; protected set; }

        /// <summary>
        /// На сколько изменилось время в секундах
        /// </summary>
        public TimeSpan CorrectionOffset { get; protected set; }

        public CounterTimeCorrectionEvent(DateTime time, Int32 n_sh)
            :base(time, n_sh)
        {
        }

        public CounterTimeCorrectionEvent(DateTime time, Int32 n_sh, TimeSpan correctionOffset)
            : base(time, n_sh)
        {
            //NewTime = newTime;
            CorrectionOffset = correctionOffset;
        }
    }
}
