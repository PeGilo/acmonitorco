﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    using ACMonitorCo.DataAccess;
    using ACMonitorCo.Logic.Parsers;

    public static class DataParser
    {
        public static IEnumerable<CounterEvent> Parse(IEnumerable<EVLOGItem> records)
        {
            LinkedList<EVLOGItem> list = new LinkedList<EVLOGItem>(records);

            // creating chain of parser that will be trying to parse records
            EVLOGParser itemParser = new SurgeEVLOGParser(
                                         new SwitchOnEVLOGParser(
                                             new SwitchOffEVLOGParser(
                                                 new PhaseFailureEVLOGParser(
                                                     new PowerOffEVLOGParser(
                                                         new PowerOnEVLOGParser(
                                                             new CounterTimeCorrectionEVLOGParser(null)))))));

            // The parsers will remove items that they can't parse
            // so in the end count of items will always be zero.
            CounterEvent ev = null;
            while (list.Count != 0)
            {
                ev = itemParser.Parse(list);
                if (ev != null)
                    yield return ev;
            }
        }

        public static IEnumerable<RtuEvent> Parse(IEnumerable<EVLOGRTUItem> records)
        {
            LinkedList<EVLOGRTUItem> list = new LinkedList<EVLOGRTUItem>(records);
            EVLOGRTUParser parser = new RtuTimeCorrectionEVLOGRTUParser(null);

            RtuEvent ev = null;
            while (list.Count != 0)
            {
                ev = parser.Parse(list);
                if (ev != null)
                    yield return ev;
            }
        }

        public static IEnumerable<CounterEvent> Parse(IEnumerable<INTERVItem> records)
        {
            LinkedList<INTERVItem> list = new LinkedList<INTERVItem>(records);
            INTERVParser parser = new INTERVParser();

            CounterEvent ev = null;
            while (list.Count != 0)
            {
                ev = parser.Parse(list);
                if (ev != null)
                    yield return ev;
            }
        }
    }
}
