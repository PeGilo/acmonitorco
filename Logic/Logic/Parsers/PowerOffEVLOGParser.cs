﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class PowerOffEVLOGParser : PowerFailureEVLOGParser
    {
        // Коды событий которые должны составлять событие "пропадание питания"
        private static int[] codeSetStart = new int[] { 0 };
        private static int[] codeSetEnd = new int[] { 0 };

        public PowerOffEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        protected override int[] CodeSetStart
        {
            get { return codeSetStart; }
        }

        protected override int[] CodeSetEnd
        {
            get { return codeSetEnd; }
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 1)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к пропаданию питания
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new PowerFailureEvent(firstItem.EVTIME, firstItem.N_SH, Switching.Off);
                    }
                }
            }
            return base.Parse(list);
        }
    }
}
