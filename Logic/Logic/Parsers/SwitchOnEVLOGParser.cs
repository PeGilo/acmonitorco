﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class SwitchOnEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "включение"
        private static int[] codeSetStart = new int[] { 208, 209, 210, 211, 212, 213 };
        private static int[] codeSetEnd = new int[] { 209, 211, 213 };

        public SwitchOnEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 3)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к включению
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new SwitchingEvent(firstItem.EVTIME, firstItem.N_SH, Switching.On);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти 6 сообщений в начале
            IEnumerable<EVLOGItem> eventsStart = (from l in list
                                                  where
                                                     (l.N_SH == firstItem.N_SH
                                                      && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                                                      && codeSetStart.Contains<int>(l.EVTYPE)
                                                     )
                                                  select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            if (eventsStart.Count<EVLOGItem>() == 6)
            {
                // Удалить обработанные события из списка
                list.RemoveItems<EVLOGItem>(eventsStart);

                RemoveEndEvents(list, firstItem);

                return true;
            }

            // Попробовать найти 3 сообщения
            eventsStart = (from l in list
                          where
                             (l.N_SH == firstItem.N_SH
                              && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                              && codeSetEnd.Contains<int>(l.EVTYPE)
                             )
                          select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            if (eventsStart.Count<EVLOGItem>() == 3)
            {
                // Удалить обработанные события из списка
                list.RemoveItems<EVLOGItem>(eventsStart);

                RemoveEndEvents(list, firstItem);

                return true;
            } 
            return false;
        }

        private static void RemoveEndEvents(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти 3 сообщения в конце (необязательно)
            DateTime secondTimeStart = firstItem.EVTIME;
            DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(15);

            IEnumerable<EVLOGItem> eventsEnd = (from l in list
                                                where
                                                    (l.N_SH == firstItem.N_SH
                                                     && (l.EVTIME > secondTimeStart)
                                                     && l.EVTIME < secondTimeEnd
                                                     && codeSetEnd.Contains<int>(l.EVTYPE)
                                                    )
                                                select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());

            if (eventsEnd.Count<EVLOGItem>() == 3)
            {
                // Удалить обработанные события из списка
                list.RemoveItems<EVLOGItem>(eventsEnd);
            }
        }
    }
}
