﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public abstract class PowerFailureEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "восстановление питания"
        protected abstract int[] CodeSetStart { get; }
        protected abstract int[] CodeSetEnd { get; }

        public PowerFailureEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        protected bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти сообщение о восстановлении питания в начале
            IEnumerable<EVLOGItem> eventsStart = (from l in list
                                                  where
                                                     (l.N_SH == firstItem.N_SH
                                                      && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                                                      && CodeSetStart.Contains<int>(l.EVTYPE)
                                                     )
                                                  select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            if (eventsStart.Count<EVLOGItem>() > 0)
            {
                // Удалить обработанные события из списка
                list.RemoveItems<EVLOGItem>(eventsStart);

                // Найти подтверждающее сообщение в конце (необязательно)
                DateTime secondTimeStart = firstItem.EVTIME;
                DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(15);

                IEnumerable<EVLOGItem> eventsEnd = (from l in list
                                                    where
                                                        (l.N_SH == firstItem.N_SH
                                                         && (l.EVTIME > secondTimeStart)
                                                         && l.EVTIME < secondTimeEnd
                                                         && CodeSetEnd.Contains<int>(l.EVTYPE)
                                                        )
                                                    select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());

                if (eventsEnd.Count<EVLOGItem>() > 0)
                {
                    // Удалить обработанные события из списка
                    list.RemoveItems<EVLOGItem>(eventsEnd);
                }

                return true;
            }
            return false;
        }
    }

}
