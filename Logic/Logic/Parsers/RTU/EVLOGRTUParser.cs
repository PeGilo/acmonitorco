﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ACMonitorCo.Logic
{
    using ACMonitorCo.DataAccess;

    /// <summary>
    /// Base class for all parsers in chain (Chain of Responsibility)
    /// </summary>
    public abstract class EVLOGRTUParser
    {
        protected EVLOGRTUParser m_successor;
        protected readonly TimeSpan TIME_PREC = TimeSpan.FromSeconds(2);

        public EVLOGRTUParser(EVLOGRTUParser successor)
        {
            m_successor = successor;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        /// <remarks>Can't parse item - call base class, or remove it if no successors</remarks>
        public virtual RtuEvent Parse(LinkedList<EVLOGRTUItem> list)
        {
            if (m_successor != null)
            {
                return m_successor.Parse(list);
            }
            else
            {
                // if you cant parse first item - remove it
                if (list != null && list.Count > 0)
                {
                    list.RemoveFirst();
                }
                return null;
            }
        }
    }
}
