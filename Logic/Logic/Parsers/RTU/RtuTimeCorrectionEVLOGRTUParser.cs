﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class RtuTimeCorrectionEVLOGRTUParser : EVLOGRTUParser
    {
        // Коды событий которые должны составлять событие "отключение"
        private static int[] codeSetStart = new int[] { 103 };
        private static int[] codeSetEnd = new int[] { 104 };

        public RtuTimeCorrectionEVLOGRTUParser(EVLOGRTUParser successor)
            : base(successor)
        {
        }

        public override RtuEvent Parse(LinkedList<EVLOGRTUItem> list)
        {
            if (list != null && list.Count >= 1)
            {
                EVLOGRTUItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к коррекции времени
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVCODE))
                {
                    Int32 rtuNumber;
                    String timeCorrection;
                    if (_extractEvent(list, firstItem, out rtuNumber, out timeCorrection))
                    {
                        return new RtuTimeCorrectionEvent(firstItem.EVTIME, rtuNumber, timeCorrection);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGRTUItem> list, EVLOGRTUItem firstItem, 
            out Int32 rtuNumber, out string timeCorrection)
        {
            // Определить номер RTU по содержимому события
            if(_extractRtuNumber(firstItem.TXT, out rtuNumber))
            {
            // Найти 1 сообщение в начале
            //EVLOGItem eventStart = (from l in list
            //                        where
            //                            (l.N_SH == firstItem.N_SH
            //                            && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
            //                            && codeSetStart.Contains<int>(l.EVTYPE)
            //                            )
            //                        select l).FirstOrDefault();

            //if (firstItem != null)
            //{
                if (!_extractTimeCorrection(firstItem.TXT, out timeCorrection))
                {
                    timeCorrection = "";
                }

                // Удалить обработанное событие из списка
                list.Remove(firstItem);

                

                //// Найти 1 сообщениt в конце (необязательно)
                //DateTime secondTimeStart = firstItem.EVTIME;
                //DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(1);

                //EVLOGItem eventEnd = (from l in list
                //                      where
                //                        (
                //                            //&& (l.EVTIME > secondTimeStart)
                //                            //&& l.EVTIME < secondTimeEnd
                //                            codeSetEnd.Contains<int>(l.EVTYPE)
                //                        )
                //                      select l).FirstOrDefault();

                //if (eventEnd != null)
                //{
                //    // Удалить обработанные события из списка
                //    list.Remove(eventEnd);
                //}

                return true;
            }
            timeCorrection = String.Empty;
            return false;
        }

        private bool _extractRtuNumber(string text, out int rtuNumber)
        {
            Regex rx = new Regex(@"УСПД:\d+");
            MatchCollection matches = rx.Matches(text);
            foreach (Match m in matches)
            {
                String digits = new String(m.Value.SkipWhile(c => !Char.IsDigit(c)).ToArray());

                if (Int32.TryParse(digits, out rtuNumber))
                {
                    return true;
                }
            }
            rtuNumber = 0;
            return false;
        }

        private bool _extractTimeCorrection(string text, out string timeCorrection)
        {
            Regex rx = new Regex(@"\(.+\)");
            MatchCollection matches = rx.Matches(text);
            if (matches.Count == 1)
            {
                timeCorrection = matches[0].Value;
                return true;
            }
            else
            {
                timeCorrection = String.Empty;
                return false;
            }
        }
    }
}
