﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic
{
    public class EVLOGFilter : EVLOGParser
    {
        public EVLOGFilter(EVLOGParser successor)
            : base(successor)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count > 0)
            {
                EVLOGItem curItem = list.First.Value;
                if (curItem.EVTYPE == 206 || curItem.EVTYPE == 207)
                {
                    list.RemoveFirst();
                    return null;
                }
            }

            return base.Parse(list);
        }
    }
}
