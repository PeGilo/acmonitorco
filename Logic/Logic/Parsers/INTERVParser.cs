﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;

namespace ACMonitorCo.Logic.Parsers
{
    public class INTERVParser
    {
        public virtual InvalidReadingEvent Parse(LinkedList<INTERVItem> list)
        {
            if (list != null && list.Count > 0)
            {
                INTERVItem item = list.First.Value;
                DateTime time = new DateTime(
                                             item.DD_MM_YYYY.Year,
                                             item.DD_MM_YYYY.Month,
                                             item.DD_MM_YYYY.Day,
                                             item.MIN_0 / 60,           // hours
                                             item.MIN_0 % 60,           // minutes
                                             0);                        // seconds
                Int32 n_sh = item.N_SH;
                Int32 interv = item.MIN_1 - item.MIN_0;
                list.RemoveFirst();
                return new InvalidReadingEvent(time, n_sh, interv);
            }
            return null;
        }
    }
}
