﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class SwitchOffEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "отключение"
        private static int[] codeSetStart = new int[] { 208, 210, 212 };
        private static int[] codeSetEnd = new int[] { 208, 210, 212 };

        public SwitchOffEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 3)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к отключению
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new SwitchingEvent(firstItem.EVTIME, firstItem.N_SH, Switching.Off);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти 3 сообщения в начале
            IEnumerable<EVLOGItem> eventsStart = (from l in list
                                                  where
                                                     (l.N_SH == firstItem.N_SH
                                                      && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                                                      && codeSetStart.Contains<int>(l.EVTYPE)
                                                     )
                                                  select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            if (eventsStart.Count<EVLOGItem>() == 3)
            {
                // Удалить обработанные события из списка
                list.RemoveItems<EVLOGItem>(eventsStart);

                // Найти 3 сообщения в конце (необязательно)
                DateTime secondTimeStart = firstItem.EVTIME;
                DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(15);

                IEnumerable<EVLOGItem> eventsEnd = (from l in list
                                                    where
                                                        (l.N_SH == firstItem.N_SH
                                                         && (l.EVTIME > secondTimeStart)
                                                         && l.EVTIME < secondTimeEnd
                                                         && codeSetEnd.Contains<int>(l.EVTYPE)
                                                        )
                                                    select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());

                if (eventsEnd.Count<EVLOGItem>() == 3)
                {
                    // Удалить обработанные события из списка
                    list.RemoveItems<EVLOGItem>(eventsEnd);
                }

                return true;
            }
            return false;
        }
    }
}
