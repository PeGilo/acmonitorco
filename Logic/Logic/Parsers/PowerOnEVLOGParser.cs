﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class PowerOnEVLOGParser : PowerFailureEVLOGParser
    {
        // Коды событий которые должны составлять событие "восстановление питания"
        private static int[] codeSetStart = new int[] { 1 };
        private static int[] codeSetEnd = new int[] { 1 };

        public PowerOnEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        protected override int[] CodeSetStart
        {
            get { return codeSetStart; }
        }

        protected override int[] CodeSetEnd
        {
            get { return codeSetEnd; }
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 1)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к восстановлению питания
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new PowerFailureEvent(firstItem.EVTIME, firstItem.N_SH, Switching.On);
                    }
                }
            }
            return base.Parse(list);
        }
    }
}
