﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class CounterTimeCorrectionEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "отключение"
        // Завершающее событие может быть в начале, если время скорректировалось назад
        private static int[] codeSetStart = new int[] { 2, 3 };
        private static int[] codeSetEnd = new int[] { 3, 2 };

        public CounterTimeCorrectionEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 1)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к коррекции времени
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    TimeSpan correctionOffset;
                    if (_extractEvent(list, firstItem, out correctionOffset))
                    {
                        return new CounterTimeCorrectionEvent(firstItem.EVTIME, firstItem.N_SH, correctionOffset);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem, out TimeSpan correctionOffset)
        {
            // Найти 1 сообщение в начале
            //EVLOGItem eventStart = (from l in list
            //                        where
            //                            (l.N_SH == firstItem.N_SH
            //                            && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
            //                            && codeSetStart.Contains<int>(l.EVTYPE)
            //                            )
            //                        select l).FirstOrDefault();

            //if (firstItem != null)
            //{
                // Запомнить время первого события
            DateTime firstTime = firstItem.EVTIME;
                // Запомнить код события, которое оказалось впереди
            int firstEventCode = firstItem.EVTYPE;
                // Удалить обработанное событие из списка
                list.Remove(firstItem);

                // Найти 1 сообщениt в конце (необязательно)
                DateTime secondTimeStart = firstItem.EVTIME.AddMinutes(-1); // Возможно время скорректировалось назад
                DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(1);

                EVLOGItem eventEnd = (from l in list
                                      where
                                        (l.N_SH == firstItem.N_SH
                                            && (l.EVTIME > secondTimeStart)
                                            && l.EVTIME < secondTimeEnd
                                            && codeSetEnd.Contains<int>(l.EVTYPE)
                                            && l.EVTYPE != firstEventCode // Если код первого события 2, то надо искать 3
                                                                          // и наоборот
                                        )
                                      select l).FirstOrDefault();

                if (eventEnd != null)
                {
                    // Расчитываем на сколько скорректировалось время и определяем по коду правильный знак коррекции.
                    if (firstEventCode == 2)
                    {
                        correctionOffset = eventEnd.EVTIME - firstTime;
                    }
                    else
                    {
                        correctionOffset = firstTime - eventEnd.EVTIME;
                    }
                    // Удалить обработанные события из списка
                    list.Remove(eventEnd);
                }
                else
                {
                    correctionOffset = new TimeSpan(0);
                }

                return true;
            //}
            //return false;
        }
    }
}
