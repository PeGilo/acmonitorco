﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    /// <summary>
    /// Проверяет событие на соответствие событию "Кратковременное включение"
    /// </summary>
    public class SurgeEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "кратковременное включение"
        private static int[] codeSetStart = new int[] { 208, 209, 210, 211, 212, 213 };
        private static int[] codeSetEnd = new int[] { 208, 210, 212 };

        /// <summary>
        /// Минимальное отставание сигнала отключения от сигнала включения (в милисек)
        /// </summary>
        private const int PERIOD_MSEC = 900;

        public SurgeEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 9)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к кратковременному включению
                if (firstItem != null &&
                    codeSetStart.Contains<int>(firstItem.EVTYPE)) 
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new SurgeEvent(firstItem.EVTIME, firstItem.N_SH);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти 6 сообщений в начале
            IEnumerable<EVLOGItem> eventsStart = (from l in list
                                                  where
                                                     (l.N_SH == firstItem.N_SH
                                                      && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                                                      && codeSetStart.Contains<int>(l.EVTYPE)
                                                     )
                                                  select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            if (eventsStart.Count<EVLOGItem>() == 6)
            {
                // Найти 3 сообщения в конце
                DateTime secondTimeStart = firstItem.EVTIME.AddMilliseconds(PERIOD_MSEC);//.AddSeconds(1);
                DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(5);

                IEnumerable<EVLOGItem> eventsEnd = (from l in list
                                                    where
                                                        (l.N_SH == firstItem.N_SH
                                                         && (l.EVTIME >= secondTimeStart)
                                                         && l.EVTIME < secondTimeEnd
                                                         && codeSetEnd.Contains<int>(l.EVTYPE)
                                                        )
                                                    select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());

                if (eventsEnd.Count<EVLOGItem>() == 3)
                {
                    // Удалить обработанные события из списка
                    list.RemoveItems<EVLOGItem>(eventsStart);
                    list.RemoveItems<EVLOGItem>(eventsEnd);

                    return true;
                }
            }
            return false;
        }
    }
}
