﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using ACMonitorCo.DataAccess;
using Utils;

namespace ACMonitorCo.Logic.Parsers
{
    public class PhaseFailureEVLOGParser : EVLOGParser
    {
        // Коды событий которые должны составлять событие "сбой фазы"
        private static int[] codeSetStart = new int[] { 208, 209, 210, 211, 212, 213 };

        public PhaseFailureEVLOGParser(EVLOGParser successor)
            : base(successor)
        {
        }

        public override CounterEvent Parse(LinkedList<EVLOGItem> list)
        {
            if (list != null && list.Count >= 1)
            {
                EVLOGItem firstItem = list.First.Value;
                // Проверить что EVTYPE имеет отношение к сбою фазы
                if (firstItem != null && codeSetStart.Contains<int>(firstItem.EVTYPE))
                {
                    if (_extractEvent(list, firstItem))
                    {
                        return new PhaseFailureEvent(firstItem.EVTIME, firstItem.N_SH);
                    }
                }
            }
            return base.Parse(list);
        }

        private bool _extractEvent(LinkedList<EVLOGItem> list, EVLOGItem firstItem)
        {
            // Найти cообщения в начале, сигнализирующие о сбое фазы
            IEnumerable<EVLOGItem> eventsStart = (from l in list
                                                  where
                                                     (l.N_SH == firstItem.N_SH
                                                      && (l.EVTIME - firstItem.EVTIME).Duration() < TIME_PREC
                                                      && codeSetStart.Contains<int>(l.EVTYPE)
                                                     )
                                                  select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());
            // Проверяется просто наличие подходящих сообщений, так как
            // предполагается, что сообщения о нормальном включении/отключении уже обработаны.
            if (eventsStart.Count<EVLOGItem>() > 0 )
            {
                // Найти подтверждающие сообщения в конце (необязательно)
                DateTime secondTimeStart = firstItem.EVTIME;
                DateTime secondTimeEnd = firstItem.EVTIME.AddMinutes(15);

                // Подтверждающие сообщения должны иметь те же коды, что и начальные.
                // Копируем список, чтобы он не очистился при удалении событий из списка
                IEnumerable<int> codeSetEnd = new List<int>(from e in eventsStart
                                              select e.EVTYPE);

                // Удалить обработанные события из списка
                // Удалить сообщения до поиска завершающих сообщений
                list.RemoveItems<EVLOGItem>(eventsStart);

                IEnumerable<EVLOGItem> eventsEnd = (from l in list
                                                    where
                                                        (l.N_SH == firstItem.N_SH
                                                         && (l.EVTIME > secondTimeStart)
                                                         && l.EVTIME < secondTimeEnd
                                                         && codeSetEnd.Contains<int>(l.EVTYPE)
                                                        )
                                                    select l).Distinct<EVLOGItem>(EVLOGItem.GetEqualityComparerByCode());

                if (eventsEnd.Count<EVLOGItem>() > 0)
                {
                    // Удалить обработанные события из списка
                    list.RemoveItems<EVLOGItem>(eventsEnd);
                }

                return true;
            }
            return false;
        }
    }
}
