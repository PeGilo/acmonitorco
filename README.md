# Reporting app #
Utility app for delivering regular reports by e-mail. The app connects to Oracle database, extracts actual data, composes report and mails it to specified emails.

This is a windows application on the .NET platform (3.5).

Business logic and data access logic are separated to different layers. The BL layer is thoroughly tested with Unit tests.

Developer environment: **C# (.NET Framework 3.5), Visual Studio**.

Used:

 * **XSLT Transformation** - for convenient reports' representation changing in runtime;
 * MS Enterprise Library **Logging Application Block** for flexible and configurable logging policy;
 * MS Enterprise Library **Exception Handling Application Block** for flexible exception handling.
 * **Oracle .NET Provider** for access to Oracle 9i database.
 * Visual Studio **Unit Testing Framework**